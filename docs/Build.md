# 1 本地编译

##    1.1 基本环境(必备)

​    JDK:11+、Maven:3.5+、MYSQL:8+、Node:16+


##    1.2 后台系统工程(JAVA端)

###     1.2.1 下载代码

```
git clone https://gitee.com/yuzhou-big-data-laboratory/lanius-orchestrate-server.git
```

###     1.2.2 打开项目，目录结构如下：
![server-project-structure.png](img/build/fast/server-project-structure.png)

### 1.2.3 初始化数据库

#### 1.2.3.1 新建数据库 lanius_orchestrate

#### 1.2.3.2 导入数据库，文件路径如下：

db/lanius_orchestrate_latest.sql

![sql-dir.png](img/build/fast/sql-dir.png)

###     1.2.4 在orchestrate-common模块的pom.xml中添加mysql依赖

```
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```


###     1.2.5 执行打包编译脚本，路径如下

![server-complie-package.png](img/build/fast/server-complie-package.png)

```
双击脚本：  package.bat
```

### 1.2.6 查看打包编译日志信息，路径如下

![server-build-log.png](img/build/fast/server-build-log.png)

### 1.2.7 打包编译后输出目录
![server-package-output-dir.png](img/build/fast/server-package-output-dir.png)

### 1.2.8 输出目录结构

![server-package-outpu-dir-structure.png](img/build/fast/server-package-outpu-dir-structure.png)

### 1.2.8 配置开发环境数据库信息，文件路径如下：
![server-dev-yml.png](img/build/fast/server-dev-yml.png)
![dev-db-yml.png](img/build/fast/dev-db-yml.png)

### 1.2.9 启动程序，启动程序路径如下：

进入目录/dist

#### 1.2.9.1 启动程序脚本


```
双击脚本 startup.cmd
```

启动成功
![server-start-success.png](img/build/fast/server-start-success.png)

#### 1.2.9.2 如果启动过程中窗口关闭

使用管理员身份运行脚本startup.cmd

![server-admin-run-startup.png](img/build/fast/server-admin-run-startup.png)

![server-admin-run-startup-cmd.png](img/build/fast/server-admin-run-startup-cmd.png)

这样可以查看到，后端程序启动时的错误日志信息

#### 1.2.9.3 查看后端程序启动的日志信息，路径如下

![sever-run-log.png](img/build/fast/sever-run-log.png)

#### 1.2.9.4 停止程序脚本


```
双击该脚本 shutdown.cmd
```


##    1.3 后台前端工程(VUE)

###     1.3.1 请确保本地已经安装node

###     1.3.2 下载源码

```
 git clone https://gitee.com/yuzhou-big-data-laboratory/lanius-orchestrate-client.git
```

###     1.3.3 打开项目，目录结构如下：
![client-structure.png](img/build/fast/client-structure.png)
###     1.3.4 执行打包编译脚本：

```
双击脚本：  package.bat
```

在文件夹下

![client-structure-package.png](img/build/fast/client-structure-package.png)

### 1.3.5 打包编译后输出目录

![client-output-dir.png](img/build/fast/client-output-dir.png)

### 1.3.6 启动程序，启动程序路径如下：

进入目录/bin

目录结构如下：
![client-start-structure.png](img/build/fast/client-start-structure.png)

```
双击脚本：  startup.cmd
```

##    1.5 浏览访问

###      1.5.1 超级管理平台登录

​			http://localhost:3000/admin/login

​            默认超管账号密码：admin/123QWEqwe!@#
![admin-login.png](img/build/fast/admin-login.png)
###      1.5.2 普通用户登录

​            http://localhost:3000/login
![auser-login.png](img/build/fast/user-login.png)
