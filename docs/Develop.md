# 1 本地开发

##    1.1 基本环境

​    JDK:11+、Maven:3.5+、MYSQL:8+、Node:16+、npm:8.16.0+、vue/cli:5+

##    1.2 开发环境

​    idea、vscode

##    1.3 后台系统工程(JAVA端)

###     1.3.1 下载代码

```
git clone https://gitee.com/yuzhou-big-data-laboratory/lanius-orchestrate-server.git
```

###     1.3.2 打开项目加载依赖目录如下：
![project-module.png](img/develop/project-module.png)
###     1.3.3 在orchestrate-common模块的pom.xml中添加mysql依赖

```
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```

###     1.3.4 导入数据库，配置开发环境数据库信息，文件路径如下：
![dev-yml.png](img/develop/dev-yml.png)
![dev-db-yml.png](img/develop/dev-db-yml.png)

###     1.3.5 在父级pom.xml输入命令mvn clean install 或者 idea操作 install
![parent-clean-install.png](img/develop/parent-clean-install.png)
###     1.3.6 启动程序，启动程序路径如下：
![start-program.png](img/develop/start-program.png)
##    1.4 后台前端工程(VUE)

###     1.4.1 请确保本地已经安装node

###     1.4.2 下载源码

```
 git clone https://gitee.com/yuzhou-big-data-laboratory/lanius-orchestrate-client.git
```

###     1.4.3 npm i,当前所有命令必须在当前工程目录下进行，目录结构如下：
![vue-project.png](img/develop/vue-project.png)
###     1.4.4 修改开发环境后端连接信息，文件路径如下：
![background-connect.png](img/develop/background-connect.png)
###     1.4.5 修改前端访问IP与端口，文件路径如下：
![web-access.png](img/develop/web-access.png)
###     1.4.6 在控制台输入命令：npm run dev,控制台打印出如下画面，则表示启动成功
![vue-start-success.png](img/develop/vue-start-success.png)
##    1.5 浏览访问

###      1.5.1 超级管理平台登录

​			http://localhost:3000/admin/login

​            默认超管账号密码：admin/123QWEqwe!@#
![admin-login.png](img/develop/admin-login.png)
###      1.5.2 普通用户登录

​            http://localhost:3000/login
![auser-login.png](img/develop/user-login.png)
