/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 工具类方法
 * @fileName tool.ts
 * @author zoujunjie
 * @date 2022-4-28
 */
const createGuid = (hasLine: Boolean = true): String => {
  function s4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }
  let line = hasLine ? "-" : "";
  return `${s4()}${s4()}${line}${s4()}${line}${s4()}${line}${s4()}${line}${s4()}${s4()}${s4()}`;
};

const checkPassword = (password: string): Boolean => {
  if (!password) {
    return false;
  }
  const lowerCaseRule = /[a-z]/;
  const upperCaseRule = /[A-Z]/;
  const numberRule = /[0-9]/;
  const specialCodeRule = /[-`=\\;',.~!@#$%^&*()_+|{}:"?/<>]/;
  const lengthRule = /^.{6,20}$/;
  return (
    lowerCaseRule.test(password) &&
    upperCaseRule.test(password) &&
    numberRule.test(password) &&
    specialCodeRule.test(password) &&
    lengthRule.test(password)
  );
};

const checkIpV4 = (ip: string): Boolean => {
  if (ip && ip.trim()) {
    let arr = ip.split(".");
    if (arr.length == 4) {
      for (let i = 0; i < arr.length; i++) {
        const item: string = arr[i];
        const num: Number = Number(item);
        if (num >= 0 && num <= 255) {
          if (num.toString().length == item.length) {
            continue;
          }
        }
        return false;
      }
      return true;
    }
  }
  return false;
};

export { createGuid, checkPassword, checkIpV4 };
