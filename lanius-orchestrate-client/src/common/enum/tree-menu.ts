/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
/**
 * @desc 树数据结构TS
 * @fileName tree-menu.ts
 * @author zhouyongjie@yzbdl.ac.cn
 * @date 2022-4-27
 */

interface IMenuItem {
  text: String;
  key: String;
  checked: Boolean;
  children?: IMenuItem[];
}
let treeMenu: Array<IMenuItem> = [
  {
    key: "/task-schedule",
    text: "任务调度",
    checked: false,
    children: [
      {
        text: "调度总览",
        key: "/tatistic",
        checked: false,
      },
      {
        text: "资源管理",
        key: "",
        checked: false,
        children: [
          {
            text: "主机配置",
            key: "/resource/server",
            checked: false,
          },
          {
            text: "服务节点",
            key: "/resource/serverProgram",
            checked: false,
          },
          {
            text: "资源配置",
            key: "/resource/taskResourceConfig",
            checked: false,
          },
          {
            text: "任务资源",
            key: "/resource/taskResource",
            checked: false,
          },
        ],
      },
      {
        text: "任务编排",
        key: "/task",
        checked: false,
      },
      {
        text: "任务实例",
        key: "/task/instance",
        checked: false,
      },
    ],
  },
  {
    key: "/system-management",
    text: "系统管理",
    checked: false,

    children: [
      {
        text: "角色管理",
        key: "/role",
        checked: false,
      },
      {
        text: "用户管理",
        key: "/user",
        checked: false,
      },
      {
        text: "日志管理",
        key: "/userLog",
        checked: false,
      },
    ],
  },
];
export default treeMenu;
