REM windows package

REM DEL maven-package-jar.log
DEL maven-package-jar.log

REM package
::如果不需要进行maven打包, 则使用 REM 注释掉该行
call mvn clean install -Dmaven.test.skip=true >> maven-package-jar.log

REM del dist
rmdir dist /s /q
REM create dist and sql
mkdir dist
mkdir dist\sql


::DEL maven-package-jar.log

REM copy main program and config
xcopy lanius-orchestrate\target\lanius-orchestrate-*.jar dist /s /i
xcopy lanius-orchestrate\src\main\resources\application-dev.yml dist /s
xcopy lanius-orchestrate\src\main\resources\application.yml dist /s
xcopy lanius-orchestrate\src\main\resources\logback-spring.xml dist /s

REM copy sql
xcopy db\* dist\sql /s

REM copy bin
xcopy bin\* dist\ /s

cd dist

REM run main
rename lanius-orchestrate-*.jar lanius-orchestrate-server.jar

