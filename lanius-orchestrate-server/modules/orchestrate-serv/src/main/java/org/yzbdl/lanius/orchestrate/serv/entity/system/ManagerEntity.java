package org.yzbdl.lanius.orchestrate.serv.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdFieldEntity;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * 管理员用户 Entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 13:17
 */
@Data
@SuperBuilder
@TableName(value = "lo_manager")
@AllArgsConstructor
@NoArgsConstructor
public class ManagerEntity extends IdFieldEntity implements UserDetails {
    private String userName;
    @JsonIgnore
    private String password;

    private LocalDateTime createTime;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
