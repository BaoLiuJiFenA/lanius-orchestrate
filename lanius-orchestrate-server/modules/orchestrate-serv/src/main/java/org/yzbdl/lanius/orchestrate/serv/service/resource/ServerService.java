/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource;

import org.yzbdl.lanius.orchestrate.serv.dto.resource.ServerPageDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.ServerEntity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务器信息业务接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-03-29 14:46
 */
public interface ServerService extends IService<ServerEntity> {

    /**
     * 增加服务器
     * @param serverEntity 服务器信息
     * @return true|false
     */
    Boolean addServer(ServerEntity serverEntity);

    /**
     * 删除服务器
     * @param id 待删除数据id
     * @return true|false
     */
    Boolean deleteServer(Long id);

    /**
     * 根据id获取服务器详细
     * @param id id
     * @return 服务器详细
     */
    ServerEntity getServer(Long id);

    /**
     * 分页查询数据
     * @param page 页码
     * @param size 单页数据量
     * @param serverPageDto 分页参数
     * @return 服务器列掉
     */
    IPage<ServerEntity> pageServer(Integer page, Integer size, ServerPageDto serverPageDto);

    /**
     * 更新服务器参数
     * @param serverEntity 更新参数
     * @return true|false
     */
    Boolean updateServer(ServerEntity serverEntity);

}
