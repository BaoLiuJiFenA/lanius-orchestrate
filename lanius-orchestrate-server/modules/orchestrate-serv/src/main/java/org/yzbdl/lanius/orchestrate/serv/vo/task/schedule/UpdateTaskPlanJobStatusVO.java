/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.task.schedule;

import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

/**
 * 定时任务状态变更
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 16:52
 */
@Data
@Builder
public class UpdateTaskPlanJobStatusVO {

    /**
     * 任务编排ID
     */
    @NotNull(message = "任务编排ID不能为空")
    private Long taskPlanId;

    /**
     * 任务编排状态
     */
    @NotNull(message = "任务编排状态不能为空")
    private Integer taskPlanStatus;

}
