/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.yzbdl.lanius.orchestrate.serv.entity.system.UserRoleEntity;

import java.util.List;

/**
 * 用户角色关联mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-12 13:47
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleEntity> {

	int insertBatch(List<UserRoleEntity> userRoleEntityList);
}
