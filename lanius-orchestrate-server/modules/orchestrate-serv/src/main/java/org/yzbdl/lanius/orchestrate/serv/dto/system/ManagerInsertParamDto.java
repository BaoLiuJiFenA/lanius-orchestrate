/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.system;

import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.password.ComplexValid;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 管理员用户增加
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 13:00
 */
@Data
public class ManagerInsertParamDto {
    @NotBlank(message = "用户名称不能为空！")
    @Size(min=2,max = 20)
    private String userName;


    @Size(max = 20, min = 6)
    @ComplexValid(message = "密码必须包含英文大小写，数字，特殊字符！")
    private String password;
}
