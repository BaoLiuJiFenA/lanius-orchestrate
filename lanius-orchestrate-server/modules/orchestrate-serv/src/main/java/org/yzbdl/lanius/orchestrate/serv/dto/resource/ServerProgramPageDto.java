package org.yzbdl.lanius.orchestrate.serv.dto.resource;

import org.yzbdl.lanius.orchestrate.serv.enums.ServerProgramStatusEnum;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseEntity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 服务节点参数类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class ServerProgramPageDto extends BaseEntity {
        
    /**
    * 节点名称
    */    
    @ApiModelProperty(value = "节点名称")
    private String programName;
        
    /**
    * 主机id
    */    
    @ApiModelProperty(value = "主机id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long serverId;
        
    /**
    * 主机端口
    */    
    @ApiModelProperty(value = "主机端口")
    private Integer programPort;
        
    /**
    * 程序类型 1-kettle 2-其他平台
    */    
    @ApiModelProperty(value = "程序类型 1-kettle 2-其他平台")
    private Integer category;
        
    /**
    * 备注
    */    
    @ApiModelProperty(value = "备注")
    private String remark;
        
    /**
    * 节点配置 1-主节点 2-从节点
    */    
    @ApiModelProperty(value = "节点配置 1-主节点 2-从节点")
    private Integer programConfig;
        
    /**
    * 节点状态 0-停止 1-运行中 2-异常
    * @see ServerProgramStatusEnum
    */    
    @ApiModelProperty(value = "节点状态 0-停止 1-运行中 2-异常")
    private Integer status;
        
    /**
    * 认证配置
    */    
    @ApiModelProperty(value = "认证配置")
    private String authConfig;
                        
    /**
    * 是否删除
    */    
    @ApiModelProperty(value = "是否删除")
    private Boolean deleted;

    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    private Long orgId;
    
}

