/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.task;

import lombok.Data;

/**
 * 调度任务中间参数
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 14:39
 */
@Data
public class TaskJobVO {

	/**
	 * 任务名称
	 */
	private String jobName;

	/**
	 * 任务分组名称
	 */
	private String jobGroupName;
}
