/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.serv.entity.system.MenuEntity;

/**
 * 菜单服务接口
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 17:31
 */
public interface MenuService extends IService<MenuEntity> {

	/**
	 * 根据权限返回菜单
	 * @param permissionIds 权限ids
	 * @return 菜单列表
	 */
	//List<MenuVo> listMenuByPermissionIds(List<Long> permissionIds);

//	/**
//	 * 保存菜单
//	 * @param menuEntity 菜单实体
//	 * @return 成功与否
//	 */
//	boolean saveMenu(MenuEntity menuEntity);
//
//	/**
//	 * 更新菜单
//	 * @param menuEntity 菜单实体
//	 * @return 成功与否
//	 */
//	boolean updateMenu(MenuEntity menuEntity);

	/**
	 * 全部菜单树
	 * @return 菜单树
	 */
	//List<MenuTreeDto> buildAllMenuTree();
}
