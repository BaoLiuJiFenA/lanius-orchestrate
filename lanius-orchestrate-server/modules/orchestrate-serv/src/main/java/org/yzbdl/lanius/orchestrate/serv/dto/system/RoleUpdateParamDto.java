/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.system;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.yzbdl.lanius.orchestrate.serv.entity.system.RoleEntity;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 角色修改DTO
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-15 13:32
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleUpdateParamDto {
	@OprLabel
	@NotBlank(message = "角色名称不能为空")
	@Size(max=64,min = 1)
	private String roleName;

	public RoleEntity coverToRoleEntity(Long id){
		return RoleEntity.builder().roleName(roleName).id(id).build();
	}

}
