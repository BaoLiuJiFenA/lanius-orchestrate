package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

/**
 * 任务状态枚举类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:12
 */
@Getter
public enum TaskStatusEnum {

    /**
     * 任务状态枚举类
     */
    DISABLED(1, "已禁用"), ENABLED(2, "已启用");

    private final Integer code;

    private final String name;

    TaskStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }


}
