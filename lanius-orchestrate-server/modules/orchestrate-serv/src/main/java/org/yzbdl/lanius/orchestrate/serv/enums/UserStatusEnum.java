package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022/2/21
 */
@Getter
@AllArgsConstructor
public enum UserStatusEnum {

    /**
     * 用户状态
     */
    NORMAL(1, "正常"),
    NO_ORGANIZATION(2, "未属于组织");

    Integer code;
    String value;

    public static UserStatusEnum findValueByCode(Integer code){
        return Optional.ofNullable(code).flatMap(param -> Stream.of(UserStatusEnum.values()).filter(w -> w.code.equals(param)).findFirst()).orElse(null);
    }

}
