/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

import java.util.Objects;

/**
 * 转换作业枚举类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-26 11:03
 */
@Getter
public enum TransJobStatusEnum {

    /**
     * 转换作业枚举类
     */
    EXEC_ERROR(1, "Exec error"), FINISHED(0, "Finished"), FINISHED_WITH_ERRORS(1, "Finished (with errors)"),
    INIT_TRANS(3, "Initializing"), INIT_JOB(10, "Waiting"), PREPARING(10, "Preparing executing"), RUNNING(4, "Running"),
    HALTING(4, "Halting"), PAUSED(8, "Paused"), STOPPED(9, "Stopped");

    private final Integer code;
    private final String name;

    TransJobStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据名称获取code
     *
     * @param name
     *        名称
     * @return
     *        code
     */
    public static Integer matchName(String name){
        for (TransJobStatusEnum statusEnum : TransJobStatusEnum.values()){
            String enumName = statusEnum.getName();
            if (Objects.equals(enumName, name)){
                return statusEnum.getCode();
            }
        }
        return null;
    }

}
