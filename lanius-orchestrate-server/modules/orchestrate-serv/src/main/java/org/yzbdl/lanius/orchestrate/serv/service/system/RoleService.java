/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.serv.dto.system.RoleInsertParamDto;
import org.yzbdl.lanius.orchestrate.serv.dto.system.RoleListDto;
import org.yzbdl.lanius.orchestrate.serv.entity.system.RoleEntity;
import org.yzbdl.lanius.orchestrate.serv.vo.system.RoleVo;

import java.util.List;

/**
 * 角色服务
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 16:47
 */
public interface RoleService extends IService<RoleEntity> {

	//List<Long> getRoleIdsByPermissionId(Long permissionId);

	/**
	 * 根据角色id保存权限
	 * @param roleId 角色id
	 * @param permissionIds 权限id集合
	 * @return 更改数量
	 */
	int savePermissionIdsByRoleId(Long roleId,List<Long> permissionIds);

	/**
	 * 角色查询
	 * @param roleListDto 角色查询条件
	 * @param page 分页
	 * @return 角色分页列表
	 */
	Page<RoleVo> listRolePage(RoleListDto roleListDto , Page<RoleVo> page);

	/**
	 * 保存角色
	 * @param roleInsertParamDto 角色保存数据
	 * @return 更改行数
	 */
	int saveRole(RoleInsertParamDto roleInsertParamDto);

	/**
	 * 删除角色
	 * @param roleId 角色id
	 * @return 更改行数
	 */
	int deleteRoleById(Long roleId);

	/**
	 * 修改角色
	 * @param roleEntity 角色实体
	 * @return 布尔值
	 */
	boolean updateRole(RoleEntity roleEntity);

}
