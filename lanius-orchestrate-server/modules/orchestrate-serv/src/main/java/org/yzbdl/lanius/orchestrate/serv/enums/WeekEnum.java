/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

import java.util.Objects;

/**
 * 星期枚举定义类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-25 11:26
 */
@Getter
public enum WeekEnum {

    /**
     * 星期枚举定义
     */
    SECONDS("1", "星期天", "Sunday"), MONDAY("2", "星期一", "Monday"), TUESDAY("2", "星期二", "Tuesday"),
    WEDNESDAY("3", "星期三", "Wednesday"), THURSDAY("4", "星期四", "Monday"), FRIDAY("5", "星期五", "Friday"),
    SATURDAY("6", "星期六", "Saturday");

    private final String code;

    private final String name;

    private final String value;

    WeekEnum(String code, String name, String value) {
        this.code = code;
        this.name = name;
        this.value = value;
    }

    /**
     * 根据编码获取名称
     *
     * @param code
     *        编码
     * @return
     *        名称
     */
    public static String matchName(String code){
        for (WeekEnum m : WeekEnum.values()){
            if (Objects.equals(m.getCode(), code)){
                return m.getName();
            }
        }
        return null;
    }

    /**
     * 根据编码获取值
     *
     * @param code
     *        编码
     * @return
     *        值
     */
    public static String matchValue(String code){
        for (WeekEnum m : WeekEnum.values()){
            if (Objects.equals(m.getCode(), code)){
                return m.getValue();
            }
        }
        return null;
    }
}
