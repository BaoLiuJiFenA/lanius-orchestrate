/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.system;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.yzbdl.lanius.orchestrate.serv.entity.system.ManagerEntity;
import org.yzbdl.lanius.orchestrate.common.jwt.TokenDto;

/**
 * 管理员token视图
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 15:06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerTokenVo {
	private ManagerEntity user;
	private boolean manager;
	private TokenDto token;
}
