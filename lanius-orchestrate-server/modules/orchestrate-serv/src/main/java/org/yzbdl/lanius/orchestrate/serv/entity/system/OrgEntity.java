package org.yzbdl.lanius.orchestrate.serv.entity.system;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdFieldEntity;

import java.time.LocalDateTime;

/**
 * 组织 org Entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 12:59
 */
@Data
@SuperBuilder
@TableName(value = "lo_org")
@AllArgsConstructor
@NoArgsConstructor
public class OrgEntity extends IdFieldEntity {
    private String orgName;
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private Boolean frozen;
}
