/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.task;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskPlanGroup;

import java.util.List;

/**
 * 任务编排分组树形结构响应页面实体
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-08 9:16
 */
@Data
public class TaskPlanGroupVO extends TaskPlanGroup {

    @TableField(exist = false)
    @ApiModelProperty(value = "分组下任务编排的统计")
    private String taskPlanCount;

    @TableField(exist = false)
    @ApiModelProperty(value = "是否为子节点")
    private Boolean isChild = false;

    @TableField(exist = false)
    @ApiModelProperty(value = "子级")
    private List<TaskPlanGroupVO> children;

    @ApiModelProperty(value = "层级")
    private Integer level;
}
