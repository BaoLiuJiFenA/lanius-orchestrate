package org.yzbdl.lanius.orchestrate.serv.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdFieldEntity;

/**
 * 权限 Entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 13:06
 */
@Data
@TableName(value = "lo_permission")
public class PermissionEntity extends IdFieldEntity {
    private String permissionName;
    private String permissionCode;
//    private String url;
//    private String method;
    private Long menuId;
    private int sort;
}
