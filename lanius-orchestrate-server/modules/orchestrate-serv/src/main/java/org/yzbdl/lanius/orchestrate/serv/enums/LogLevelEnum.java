package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

/**
 * 日志等级枚举类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:12
 */
@Getter
public enum LogLevelEnum {

    /**
     * 日志等级枚举
     */
    NOTHING(0, "无(Nothing)"), ERROR(1, "错误(ERROR)"), MINIMAL(2, "最小(Minimal)"), BASIC(3, "基本(Basic)"),
    DETAILED(4, "详细(Detailed)"), DEBUG(5, "调试(Debug)"), ROWLEVEL(6, "行级(Row level)");

    private final Integer code;

    private final String name;

    LogLevelEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

}
