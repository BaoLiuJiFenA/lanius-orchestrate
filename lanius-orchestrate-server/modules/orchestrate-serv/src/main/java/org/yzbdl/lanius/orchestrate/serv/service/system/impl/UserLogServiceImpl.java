/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.serv.dto.system.UserLogQueryDto;
import org.yzbdl.lanius.orchestrate.serv.entity.system.UserLogEntity;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.UserLogMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.UserLogService;
import org.yzbdl.lanius.orchestrate.serv.vo.system.UserLogVo;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.utils.SpecialCharacterUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户日志接口实现
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-13 15:33
 */
@Service
public class UserLogServiceImpl extends ServiceImpl<UserLogMapper, UserLogEntity> implements UserLogService {

	/**
	 * 获取日志事件
	 * @return 返回事件名称
	 */
	@Override
	public List<String> getAllEventName() {
		Field[] fields = UserOprEventConstant.class.getDeclaredFields();
		List<String> eventNames = new ArrayList<>();
		try{
			for(Field field:fields){
				eventNames.add(field.get(UserOprEventConstant.class).toString());
			}
		}catch (IllegalAccessException e){
			throw new RuntimeException("任务事件获取失败！");
		}
		return eventNames;
	}


	/**
	 * 获取用户日志分页列表
	 * @param page 分页对象
	 * @param userLogQueryDto 用户日志条件
	 * @return 分页问题
	 */
	@Override
	public Page<UserLogVo> listUserLogsPage(IPage<UserLogEntity> page, UserLogQueryDto userLogQueryDto) {
		LambdaQueryWrapper<UserLogEntity> queryWrapper = new LambdaQueryWrapper<UserLogEntity>()
				.eq(StringUtils.isNotEmpty(userLogQueryDto.getEventName()),UserLogEntity::getEventName,userLogQueryDto.getEventName())
				.gt(userLogQueryDto.getBeginTime()!=null,UserLogEntity::getCreateTime,userLogQueryDto.getBeginTime())
				.lt(userLogQueryDto.getEndTime()!=null,UserLogEntity::getCreateTime,userLogQueryDto.getEndTime());

		return baseMapper.queryUserLogByNickName(
				SpecialCharacterUtil.escapeStr(userLogQueryDto.getNickName()),
				queryWrapper,
				page
		);

	}



}
