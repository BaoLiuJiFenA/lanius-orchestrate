/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 增量日志查询DTO
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-21 13:37
 */
@Data
public class IncrLogQueryDTO {

	@NotNull(message = "任务编排ID不能为空")
    @ApiModelProperty(value = "任务编排ID")
    private Long taskPlanId;

	@NotNull(message = "任务实例ID不能为空")
	@ApiModelProperty(value = "任务实例ID")
    private Long taskInstanceId;

}
