/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.task.schedule;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.yzbdl.lanius.orchestrate.common.annotation.valid.cron.CronValid;

import lombok.Builder;
import lombok.Data;

/**
 * 添加任务编排任务编排
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 16:02
 */
@Data
@Builder
public class AddTaskPlanJobVO {

    /**
     * 任务编排ID
     */
    @NotNull(message = "任务编排ID不能为空")
    private Long taskPlanId;

    /**
     * 任务编排分组ID
     */
    private Long taskPlanGroupId;

    /**
     * 定时任务的cron表达式
     */
    @NotBlank(message = "任务编排的cron表达式不能为空")
    @CronValid(message = "cron表达式格式不正确")
    private String cron;

    /**
     * 节点id
     */
    private Long nodeId;

    /**
     * 是否异步
     */
    private Boolean async;

    /**
     * 启动任务后是否暂停（定时任务服务初始化时使用，禁用状态的计划启动后要暂停）
     */
    private Boolean isPause;

    /**
     * 动态的JOB分组名称
     */
    private String jobGroupName;

    /**
     * 任务分组名称
     */
    private String taskGroupName;

    /**
     * 是否是增量日志
     */
    private Boolean isIncrementalLog;

    /**
     * 是否并行处理
     */
    private Boolean isParallel;
}
