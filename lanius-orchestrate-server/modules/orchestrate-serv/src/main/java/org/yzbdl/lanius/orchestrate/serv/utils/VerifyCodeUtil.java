/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.utils;

import org.yzbdl.lanius.orchestrate.serv.dto.system.VerifyCodeDto;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 验证码工具
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-25 13:38
 */
public class VerifyCodeUtil {

	public static ConcurrentHashMap<String,VerifyCodeDto> verifyCodeMap = new ConcurrentHashMap<String, VerifyCodeDto>();

	public static void addCode(String key,VerifyCodeDto verifyCodeDto){
		clearExpireCode();
		verifyCodeMap.put(key,verifyCodeDto);
	}

	public static void clearExpireCode(){
		for(Map.Entry<String,VerifyCodeDto> entry: verifyCodeMap.entrySet()){
			if(entry.getValue().getExpireDate().isBefore(LocalDateTime.now())){
				verifyCodeMap.remove(entry.getKey());
			}
		}
	}

	public static boolean verify(String key,String code){
		clearExpireCode();
		return verifyCodeMap.containsKey(key)&&verifyCodeMap.get(key).getLineCaptcha().verify(code);
	}


}
