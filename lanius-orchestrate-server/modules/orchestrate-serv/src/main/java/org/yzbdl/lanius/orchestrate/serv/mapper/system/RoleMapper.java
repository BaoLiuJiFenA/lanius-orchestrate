/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.serv.entity.system.RoleEntity;
import org.yzbdl.lanius.orchestrate.serv.vo.system.RoleVo;

import java.util.List;

/**
 * 角色mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 16:46
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleEntity> {


	//public List<RoleEntity> getRolesByMenuId(Long menuId);

	int countRoleVo(@Param("ew") LambdaQueryWrapper<RoleEntity> wapper);

	List<RoleVo> queryRoleVo(@Param("skip") Long skip, @Param("pageSize") Long pageSize, @Param("ew") LambdaQueryWrapper<RoleEntity> wapper);
}
