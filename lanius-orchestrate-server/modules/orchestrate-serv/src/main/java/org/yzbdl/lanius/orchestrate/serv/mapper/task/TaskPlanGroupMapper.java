/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskPlanGroup;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskPlanGroupVO;

import java.util.List;

/**
 * 任务编排分组mapper
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 16:03
 */
@Mapper
public interface TaskPlanGroupMapper extends BaseMapper<TaskPlanGroup> {


    /**
     * 分组树形结构并统计任务编排数量
     *
     * @return
     *        结果信息
     */
    List<TaskPlanGroupVO> treeList();

    /**
     * 根据id获取所有的上级
     *
     * @param id
     *        分组ID
     * @return
     *        上级
     */
    List<TaskPlanGroup> getParentsById(@Param("id") Long id);
}
