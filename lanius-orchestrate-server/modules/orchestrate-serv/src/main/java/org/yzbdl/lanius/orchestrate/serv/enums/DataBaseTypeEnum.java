/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 资源数据库类型枚举
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-12 14:26
 */
@AllArgsConstructor
@Getter
public enum DataBaseTypeEnum {

    /**
     * 资源数据库类型枚举
     */
    MYSQL(1, "mysql", "jdbc:mysql://"), ORACLE(2, "oracle", ""), MYSQL8(3, "mysql8", ""), SQLSERVER(4, "MSSQL", ""),
    SQLSERVERNATIVE(5, "MSSQLNATIVE", ""), POSTGRESQL(6, "postgresql", ""), KINGBASEES8(7, "kingbase8", ""),
    KINGBASEES(8, "kingbase", "");

    Integer code;

    String value;

    String urlPrefix;

}
