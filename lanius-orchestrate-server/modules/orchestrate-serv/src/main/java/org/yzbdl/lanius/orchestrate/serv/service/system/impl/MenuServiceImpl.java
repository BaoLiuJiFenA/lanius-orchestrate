/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.serv.entity.system.MenuEntity;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.MenuMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.MenuService;

/**
 * 菜单服务实现
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 17:32
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuEntity> implements MenuService {

	/**
	 * 根据权限返回菜单
	 * @param permissionIds 权限ids
	 * @return 菜单列表
	 */
//	@Override
//	public List<MenuVo> listMenuByPermissionIds(List<Long> permissionIds){
//		return !CurrentUserUtil.isOrgChief()?
//				baseMapper.queryMenusByPermissionIds(permissionIds):
//				new ArrayList<>();
//	}

//	/**
//	 * 全部菜单树
//	 * @return 菜单树
//	 */
//	@Override
//	public List<MenuTreeDto> buildAllMenuTree(){
//		//构建树节点
//		List<MenuTreeDto> treeNodes = list().stream()
//				.map(g -> MenuTreeDto.builder().id(g.getId()).parentId(g.getPid()).menuName(g.getMenuName()).url(g.getMenuUrl()).build())
//				.collect(Collectors.toList());
//		return TreeUtil.build(treeNodes);
//	}


//	/**
//	 * 保存菜单
//	 * @param menuEntity 菜单实体
//	 * @return 成功与否
//	 */
//	@Override
//	public boolean saveMenu(MenuEntity menuEntity){
//		return save(resetMenuPath(menuEntity));
//	}
//
//	/**
//	 * 更新菜单
//	 * @param menuEntity 菜单实体
//	 * @return 成功与否
//	 */
//	@Override
//	public boolean updateMenu(MenuEntity menuEntity){
//		return updateById(resetMenuPath(menuEntity));
//	}

//	/**
//	 * 重置菜单路径
//	 * @param menuEntity 菜单实体
//	 * @return 菜单实体
//	 */
//	private MenuEntity resetMenuPath(MenuEntity menuEntity){
//		if(menuEntity.getPid()!=null){
//			MenuEntity parentMenu = getById(menuEntity.getPid());
//			if(parentMenu!=null){
//				String prePath = StringUtils.isEmpty(parentMenu.getPath())?"":parentMenu.getPath()+",";
//				menuEntity.setPath(prePath + parentMenu.getId());
//			}
//		}
//		return menuEntity;
//	}


}
