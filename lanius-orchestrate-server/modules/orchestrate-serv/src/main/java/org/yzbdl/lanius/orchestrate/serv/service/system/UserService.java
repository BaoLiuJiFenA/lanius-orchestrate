/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.serv.dto.system.ManagerUserQueryDto;
import org.yzbdl.lanius.orchestrate.serv.dto.system.UserInsertParamDto;
import org.yzbdl.lanius.orchestrate.serv.dto.system.UserListDto;
import org.yzbdl.lanius.orchestrate.serv.dto.system.UserUpdateParamDto;
import org.yzbdl.lanius.orchestrate.serv.entity.system.UserEntity;
import org.yzbdl.lanius.orchestrate.serv.vo.system.NickNameVo;
import org.yzbdl.lanius.orchestrate.serv.vo.system.UserVo;

import java.util.List;

/**
 * 用户服务
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 14:14
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 添加用户
     * @param userInsertParamDto 用户参数模型
     * @return 用户实体
     */
    UserEntity insertUser(UserInsertParamDto userInsertParamDto);

    /**
     * 用户列表
     * @param userListDto 用户查询条件
     * @param page 分页内容
     * @return 用户分页列表
     */
    Page<UserVo> listUserPage(UserListDto userListDto, Page<UserVo> page);

    /**
     * 用户管理员查询用户
     * @param managerUserQueryDto 管理员查询条件
     * @param page 分页
     * @return 用户列表
     */
    Page<UserEntity> listUserPage(ManagerUserQueryDto managerUserQueryDto, Page<UserEntity> page);

    /**
     * 该组织是否存在用户
     * @param orgId 组织id
     * @return 布尔值
     */
    boolean isExistUserByOrgId(Long orgId);

    /**
     * 更新用户信息
     * @param userUpdateParamDto 用户更新参数
     * @return 成功与否
     */
    boolean updateUser(UserUpdateParamDto userUpdateParamDto);

    /**
     * 给组织添加用户
     * 普通用户添加不需要组织参数，这个地方仅仅是给管理员服务的
     * @param userInsertParamDto 组织对象
     * @param orgId 组织Id
     * @param orgChief 是否是组织负责人（目前超级管理员创建的用户都是组织领袖）
     * @return 用户实体
     */
    UserEntity insertUser(UserInsertParamDto userInsertParamDto, Long orgId, boolean orgChief);

    /**
     * 保存用户角色
     * @param roleIds 角色id列表
     * @param userId 用户id
     * @return 更改行数
     */
    int saveUserRoles(List<Long> roleIds, Long userId);

    /**
     * 更具ids获取用户的昵称
     * @param ids 用户id列表
     * @return 带有id和昵称的键值对
     */
    List<NickNameVo> listNickNamesByIds(List<Long> ids);

    /**
     * 通过用户名模糊查询用户列表
     * @param userName 用户名
     * @return 用户列表
     */
    List<UserEntity> listUserByUserName(String userName);

    /**
     * 重设用户密码
     * @param userId 用户id
     * @return 新的密码
     */
    String resetPassword(Long userId);

    /**
     * 设置用户状态
     * @param status 状态值
     * @param userId 用户ID
     * @return 成功与否
     */
    Boolean setUserState(int status, Long userId);

    /**
     * 删除用户
     * @param id 用户id
     * @return 更改行数
     */
    int deleteUserById(Long id,Long orgId);

    /**
     * 判断该id是否是有效的用户
     * @param userId 用户id
     * @return 布尔值
     */
    boolean isExistUserById(Long userId);
}
