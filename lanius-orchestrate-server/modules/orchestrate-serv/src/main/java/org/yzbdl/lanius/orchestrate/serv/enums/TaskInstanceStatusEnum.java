package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

import java.util.Objects;

/**
 * 任务实例状态枚举类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:12
 */
@Getter
public enum TaskInstanceStatusEnum {

    /**
     * 任务实例状态枚举类
     */
    SUCCESS(1, "成功", "Finished"), FAIL(2, "失败", "Exec error"), INIT_FAILED(3, "初始化失败", "Initializing error"),
    BE_INIT(4, "初始化中", "Initializing"), IN_SERVICE(5, "运行中", "Running"), SKIP(6, "跳过", "Skip"),
    HAVE_TO_RETRY(7, "已重试", "Have to retry"), MONITOR_EXCEPTION(8, "监控异常", "Monitor exception"),
    PAUSE_MANUAL(9, "暂停（手动）", "Paused"), STOP_MANUAL(10, "停止（手动）", "Stopped"), WAITING(11, "等待中", "Waiting");

    private final Integer code;

    private final String name;

    private final String value;

    TaskInstanceStatusEnum(Integer code, String name, String value) {
        this.code = code;
        this.name = name;
        this.value = value;
    }

    /**
     * 根据编码获取值
     *
     * @param code
     *        编码
     * @return
     *        值
     */
    public static String matchValue(Integer code){
        for (TaskInstanceStatusEnum m : TaskInstanceStatusEnum.values()){
            if (Objects.equals(m.getCode(), code)){
                return m.getValue();
            }
        }
        return null;
    }

}
