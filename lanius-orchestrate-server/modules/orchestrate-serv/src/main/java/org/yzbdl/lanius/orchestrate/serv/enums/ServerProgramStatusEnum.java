/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 服务节点状态枚举
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-12 14:01
 */
@AllArgsConstructor
@Getter
public enum ServerProgramStatusEnum {

	/**
	 * 服务节点状态
	 */
	STOPPED(0, "停止"), RUNNING(1, "运行中"),ABNORMAL(2, "异常");

	Integer code;

	String value;

}
