/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.entity.task;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdFieldEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 任务步骤日志
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-19 14:03
 */
@Data
@TableName(value = "lo_task_step_log")
public class TaskStepLog extends IdFieldEntity {

    @ApiModelProperty(value = "任务实例id")
    @TableField(value = "task_instance_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taskInstanceId;

    @ApiModelProperty(value = "日期")
    @TableField(value = "log_date")
    private String logDate;

    @ApiModelProperty(value = "转换名称")
    @TableField(value = "trans_name")
    private String transName;

    @ApiModelProperty(value = "步骤名称")
    @TableField(value = "step_name")
    private String stepName;

    @ApiModelProperty(value = "步骤复制id 一个步骤可以复制多个线程，当前线程i d")
    @TableField(value = "step_copy")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stepCopy;

    @ApiModelProperty(value = "行读")
    @TableField(value = "lines_read")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linesRead;

    @ApiModelProperty(value = "行写")
    @TableField(value = "lines_written")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linesWritten;

    @ApiModelProperty(value = "行更新")
    @TableField(value = "lines_updated")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linesUpdated;

    @ApiModelProperty(value = "行输入")
    @TableField(value = "lines_input")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linesInput;

    @ApiModelProperty(value = "行输出")
    @TableField(value = "lines_output")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linesOutput;

    @ApiModelProperty(value = "行拒绝")
    @TableField(value = "lines_rejected")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linesRejected;

    @ApiModelProperty(value = "错误")
    @TableField(value = "errors")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long errors;

    @ApiModelProperty(value = "状态描述")
    @TableField(value = "status_description")
    private String statusDescription;

    @ApiModelProperty(value = "输入缓冲行")
    @TableField(value = "input_buffer_rows")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inputBufferRows;

    @ApiModelProperty(value = "输出缓冲行")
    @TableField(value = "output_buffer_rows")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long outputBufferRows;

    @ApiModelProperty(value = "平均处理速度")
    @TableField(value = "speed")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long speed;

    @ApiModelProperty(value = "运行时间")
    @TableField(value = "running_time")
    private Double runningTime;

}
