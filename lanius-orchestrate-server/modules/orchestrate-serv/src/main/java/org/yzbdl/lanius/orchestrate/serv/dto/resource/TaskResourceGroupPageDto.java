package org.yzbdl.lanius.orchestrate.serv.dto.resource;

import org.yzbdl.lanius.orchestrate.common.base.entity.BaseEntity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 任务资源分组参数类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class TaskResourceGroupPageDto extends BaseEntity {
        
    /**
    * 分组名
    */    
    @ApiModelProperty(value = "分组名")
    private String groupName;
        
    /**
    * 组织id
    */    
    @ApiModelProperty(value = "组织id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orgId;
        
    /**
    * 父级id
    */    
    @ApiModelProperty(value = "父级id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pid;
    
}

