/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.entity.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

import javax.validation.constraints.Size;

/**
 * 任务资源分组
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("lo_task_resource_group")
public class TaskResourceGroupEntity extends BaseOrgEntity {

    /**
    * 分组名
    */
    @OprLabel
    @ApiModelProperty(value = "分组名")
    @Size(max = 64, message = "资源分组名不能超出64位字符！")
    private String groupName;

    /**
    * 父级id
    */
    @ApiModelProperty(value = "父级id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pid;

    /**
     * 是否被删除
     */
    @ApiModelProperty(value = "是否被删除")
    @JsonSerialize(using = ToStringSerializer.class)
    private Boolean deleted;

}
