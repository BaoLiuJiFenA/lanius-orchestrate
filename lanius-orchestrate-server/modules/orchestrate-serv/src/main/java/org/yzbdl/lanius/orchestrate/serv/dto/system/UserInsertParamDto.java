package org.yzbdl.lanius.orchestrate.serv.dto.system;

import lombok.Builder;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.password.ComplexValid;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 用户新增 参数
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 15:26
 */
@Data
@Builder
public class UserInsertParamDto {
    @OprLabel
    @NotBlank(message = "用户名不能为空")
    @Size(max = 64)
    private String userName;
    @NotBlank(message = "用户昵称不能为空")
    @Size(max = 64)
    private String nickName;
    @NotBlank(message = "用户密码不能为空")
    @Size(max = 32, min = 6)
    @ComplexValid(message = "密码必须包含英文大小写，数字，特殊字符！")
    private String password;

    @NotNull
    List<Long> roleIds;

    private boolean chief;

//    public UserInsertParamDto instance(){
//        return this;
//    }
}
