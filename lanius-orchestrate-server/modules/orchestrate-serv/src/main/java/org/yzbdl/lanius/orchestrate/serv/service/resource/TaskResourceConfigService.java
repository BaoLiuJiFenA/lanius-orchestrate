/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource;

import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourceConfigPageDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.TaskResourceConfigEntity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 任务资源配置信息业务接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 16:14
 */
public interface TaskResourceConfigService extends IService<TaskResourceConfigEntity> {

	/**
	 * 增加任务资源配置
	 * @param taskResourceConfigEntity 任务资源配置信息
	 * @return true|false
	 */
	Boolean addTaskResourceConfig(TaskResourceConfigEntity taskResourceConfigEntity);

	/**
	 * 删除任务资源配置
	 * @param id 待删除数据id
	 * @return true|false
	 */
	Boolean deleteTaskResourceConfig(Long id);

	/**
	 * 根据id获取任务资源配置详细
	 * @param id id
	 * @return 任务资源配置详细
	 */
	TaskResourceConfigEntity getTaskResourceConfig(Long id);

	/**
	 * 分页查询数据
	 * @param page 页码
	 * @param size 单页数据量
	 * @param taskResourceConfigPageDto 分页参数
	 * @return 任务资源配置列掉
	 */
	IPage<TaskResourceConfigEntity> pageTaskResourceConfig(Integer page, Integer size, TaskResourceConfigPageDto taskResourceConfigPageDto);

	/**
	 * 更新任务资源配置参数
	 * @param taskResourceConfigEntity 更新参数
	 * @return true|false
	 */
	Boolean updateTaskResourceConfig(TaskResourceConfigEntity taskResourceConfigEntity);

}
