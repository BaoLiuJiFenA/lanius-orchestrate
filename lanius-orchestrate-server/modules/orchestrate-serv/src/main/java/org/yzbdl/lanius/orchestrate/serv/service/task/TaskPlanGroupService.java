/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.task;

import java.util.List;

import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskPlanGroup;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskPlanGroupVO;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 任务编排分组service
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 16:01
 */
public interface TaskPlanGroupService extends IService<TaskPlanGroup> {

    /**
     * 新增任务编排分组
     *
     * @param taskPlanGroup
     *        任务编排分组
     * @return
     *        结果信息
     */
    Boolean saveEntity(TaskPlanGroup taskPlanGroup);

    /**
     * 编辑分组
     *
     * @param taskPlanGroup
     *        任务编排分组
     * @return
     *        结果信息
     */
    Boolean updateEntity(TaskPlanGroup taskPlanGroup);

    /**
     * 批量更新分组
     *
     * @param taskPlanGroups
     *        分组集合
     * @return
     *        结果信息
     */
    Boolean batchUpdateEntity(List<TaskPlanGroup> taskPlanGroups);

    /**
     * 根据id删除分组信息
     *
     * @param id
     *        分组ID
     * @return
     *        结果信息
     */
    Boolean deleteEntity(Long id);

    /**
     * 分组树形结构并统计任务编排数量
     *
     * @return
     *        结果信息
     */
    List<TaskPlanGroupVO> treeList();

    /**
     * 分组树形结构,不带数量。 任务编排新增、编辑使用
     *
     * @return
     *        结果信息
     */
    List<TaskPlanGroupVO> queryTaskGroupTreeForTaskPlan();

    /**
     * 校验任务编排分组名称是否重复
     *
     * @param taskPlanGroup
     *        任务编排分组实体参数
     * @return
     *        结果信息
     */
    Boolean checkRepeatGroup(TaskPlanGroup taskPlanGroup);
}
