/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.task;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskStepLog;

/**
 * 任务步骤日志VO
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-19 15:07
 */
@Data
public class TaskStepLogVO extends TaskStepLog {

	/**
	 * 执行时间
	 */
	private Double seconds;

	/**
	 * 缓冲等待数据量
	 */
	@ApiModelProperty(value = "缓冲等待休眠的数据量")
	private String priority;

	/**
	 * 复制的记录行数
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	private Long copy;

}
