/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.resource;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.ExternalDirectoryTreeDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.ExternalFileDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourceDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourcePageDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.TaskResourceEntity;

import java.util.List;

/**
 * 任务资源信息业务接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 16:49
 */
public interface TaskResourceService extends IService<TaskResourceEntity> {

	/**
	 * 增加任务资源
	 * @param taskResourceDto 任务资源信息
	 * @return true|false
	 */
	Boolean addTaskResource(TaskResourceDto taskResourceDto);

	/**
	 * 删除任务资源
	 * @param id 待删除数据id
	 * @return true|false
	 */
	Boolean deleteTaskResource(Long id);

	/**
	 * 根据id获取任务资源详细
	 * @param id id
	 * @return 任务资源详细
	 */
	TaskResourceEntity getTaskResource(Long id);

	/**
	 * 分页查询数据
	 * @param page 页码
	 * @param size 单页数据量
	 * @param taskResourcePageDto 分页参数
	 * @return 任务资源列掉
	 */
	IPage<TaskResourceDto> pageTaskResource(Integer page, Integer size, TaskResourcePageDto taskResourcePageDto);

	/**
	 * 根据任务资源分组查询是否存在任务资源
	 * @param taskResourceGroupId taskResourceGroupId
	 * @return true|false
	 */
	Boolean isExistsTaskResource(Long taskResourceGroupId);

	/**
	 * 更新任务资源参数
	 * @param taskResourceDto 更新参数
	 * @return true|false
	 */
	Boolean updateTaskResource(TaskResourceDto taskResourceDto);

	/**
	 * 批量更新配置文件中资源库名
	 * @param resourceConfigId 资源库id
	 * @param repoName 资源库名称
	 */
	void batchUpdateResourceContent(Long resourceConfigId, String repoName);

	/**
	 * 根据任务资源分组ID获取资源信息。任务编排新增、编辑使用
	 *
	 * @param resourceGroupId
	 *        资源分组ID
	 * @return
	 *        结果数据
	 */
	List<TaskResourceEntity> getListByGroupIdForTaskPlan(Long resourceGroupId);

	/**
	 * 获取资源树
	 * @param resourceConfigId	资源库id
	 * @return 资源树结构
	 */
	List<ExternalDirectoryTreeDto> buildExternalResourceTree(Long resourceConfigId);

	/**
	 * 获取外部树目录下内容
	 * @param directoryId 目录id
	 * @param resourceConfigId 根目录为 0
	 * @param resourceType 资源类型
	 * @return 外部树目录下内容
	 */
	ExternalFileDto getExternalResourceFile(Long directoryId, Long resourceConfigId, Integer resourceType);

}
