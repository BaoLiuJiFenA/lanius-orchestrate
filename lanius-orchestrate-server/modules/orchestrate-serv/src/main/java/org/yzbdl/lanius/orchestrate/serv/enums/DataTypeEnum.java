/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

/**
 * 数据类型
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 13:31
 */
@Getter
public enum DataTypeEnum {

    /**
     * 数据类型枚举
     */
    STRING("String", "字符串"), NUMBER("Number", "数字"), JSON_OBJECT("JsonObject", "JSON对象"), ARR("Arr", "数组"),
    BOOLEAN("Boolean", "布尔"), NULL("Null", "Null");

    private final String code;

    private final String name;

    DataTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

}
