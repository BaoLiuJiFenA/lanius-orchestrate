package org.yzbdl.lanius.orchestrate.serv.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.base.entity.IdFieldEntity;

/**
 * 菜单entity
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 12:56
 */
@Data
@TableName(value = "lo_menu")
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class MenuEntity extends IdFieldEntity {
    private String menuName;
    private String menuUrl;
}
