/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 任务实例查询实体参数
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 9:12
 */
@Data
public class TaskInstanceQueryDTO {

    @ApiModelProperty(value = "关键字查询")
    private String keyword;

    @ApiModelProperty(value = "任务实例状态")
    private Integer status;

    @ApiModelProperty(value = "任务编排ID")
    private Long taskPlanId;

    @ApiModelProperty(value = "开始日期")
    private String beginTime;

    @ApiModelProperty(value = "校色日期")
    private String endTime;
}
