/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.entity.resource;

import org.yzbdl.lanius.orchestrate.serv.enums.DataBaseTypeEnum;
import org.yzbdl.lanius.orchestrate.serv.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 任务资源
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("lo_task_resource")
public class TaskResourceEntity extends BaseOrgEntity {

    /**
     * 资源类型  1作业 2转换
     * @see ResourceTypeEnum
     */
    @ApiModelProperty(value = "资源类型 1作业 2转换")
    private Integer resourceType;

    /**
    * 备注
    */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 资源配置
    */
    @ApiModelProperty(value = "资源配置")
    private String resourceContent;

    /**
    * 资源配置id 1-mysql
    * @see DataBaseTypeEnum
    */
    @ApiModelProperty(value = "资源配置id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceConfigId;

    /**
    * 资源组id
    */
    @ApiModelProperty(value = "资源组id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;

    /**
    * 资源名称
    */
    @OprLabel
    @ApiModelProperty(value = "资源名称")
    private String resourceName;

    /**
    * 是否被删除
    */
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;

}

