/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 任务资源统计实体类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-18 10:59
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResourceStatisticDto {

	/**
	 * 主机链接正常数
	 */
	@ApiModelProperty(value = "主机链接正常数")
	private Long serverNormalCount;

	/**
	 * 主机链接异常数
	 */
	@ApiModelProperty(value = "主机链接异常数")
	private Long serverAbnormalCount;

	/**
	 * 服务节点进行中数量
	 */
	@ApiModelProperty(value = "服务节点进行中数量")
	private Long serverProgramStoppedCount;

	/**
	 * 服务节点已停止数量
	 */
	@ApiModelProperty(value = "服务节点已停止数量")
	private Long serverProgramRunningCount;

	/**
	 * 服务节点异常数量
	 */
	@ApiModelProperty(value = "服务节点异常数量")
	private Long serverProgramAbnormalCount;

	/**
	 * 资源库链接正常数量
	 */
	@ApiModelProperty(value = "资源库链接正常数量")
	private Long resourceConfigNormalCount;

	/**
	 * 资源库链接异常数量
	 */
	@ApiModelProperty(value = "资源库链接异常数量")
	private Long resourceConfigAbnormalCount;

	/**
	 * 任务资源作业数量
	 */
	@ApiModelProperty(value = "任务资源作业数量")
	private Long jobCount;

	/**
	 * 任务资源转换数量
	 */
	@ApiModelProperty(value = "任务资源转换数量")
	private Long ktrCount;

}
