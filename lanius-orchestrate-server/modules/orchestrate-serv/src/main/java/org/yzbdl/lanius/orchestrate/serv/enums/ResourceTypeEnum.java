package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.Getter;

import java.util.Objects;

/**
 * 资源类型枚举类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:12
 */
@Getter
public enum ResourceTypeEnum {

    /**
     * 资源类型枚举类
     */
    KETTLE_OBJ(1, "作业","job"), KETTLE_KTR(2, "转换","ktr");

    private final Integer code;

    private final String name;

    private final String value;

    ResourceTypeEnum(Integer code, String name,String value) {
        this.code = code;
        this.name = name;
        this.value = value;
    }


    /**
     * 根据编码获取值
     *
     * @param code
     *        编码
     * @return
     *        值
     */
    public static String matchValue(Integer code){
        for (ResourceTypeEnum m : ResourceTypeEnum.values()){
            if (Objects.equals(m.getCode(), code)){
                return m.getValue();
            }
        }
        return null;
    }

}
