/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.entity.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.serv.enums.DataBaseTypeEnum;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 任务资源配置
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("lo_task_resource_config")
public class TaskResourceConfigEntity extends BaseOrgEntity {

    /**
    * 连接名
    */
    @OprLabel
    @ApiModelProperty(value = "连接名")
    @NotEmpty(message = "连接名不能为空！")
    @Size(max = 64, message = "连接名不能超出64位字符！")
    private String connectName;

    /**
    * 链接地址
    */
    @NotEmpty(message = "链接地址不能为空！")
    @Size(max = 512, message = "链接地址不能超出512位字符！")
    @ApiModelProperty(value = "链接地址")
    private String connectUrl;

    /**
    * 链接账号
    */
    @NotEmpty(message = "链接账号不能为空！")
    @ApiModelProperty(value = "链接账号")
    private String connectAccount;

    /**
    * 链接密码
    */
    @NotEmpty(message = "链接密码不能为空！")
    @ApiModelProperty(value = "链接密码")
    private String connectPassword;

    /**
    * 备注
    */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 数据库类型 1 mysql
    * @see DataBaseTypeEnum
    */
    @NotNull(message = "数据库类型不能为空！")
    @ApiModelProperty(value = "数据库类型")
    private Integer connectCategory;

    /**
    * 是否被删除
    */
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;

    /**
     * 状态 1 - 正常，0 - 异常
     */
    @ApiModelProperty(value = "状态 1 - 正常，0 - 异常")
    private Integer status;

}

