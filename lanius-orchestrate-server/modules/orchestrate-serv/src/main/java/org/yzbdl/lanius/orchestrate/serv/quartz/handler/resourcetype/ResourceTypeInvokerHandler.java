/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.quartz.handler.resourcetype;

import org.springframework.beans.factory.InitializingBean;

import java.util.List;

/**
 * 资源类型执行器
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-20 9:36
 */
public interface ResourceTypeInvokerHandler extends InitializingBean {

    /**
     * 获取状态检查网址
     *
     * @return
     *        网址
     */
    String getCheckStatusUrl();

    /**
     * 构建请求参数
     *
     * @return
     *       请求参数
     */
    List<String> getRequestParam();

    /**
     * 获取转换、作业的图片请求地址
     *
     * @return uri
     *
     */
    String getImageUrL();

}
