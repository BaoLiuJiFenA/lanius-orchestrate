/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.utils;

import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.yzbdl.lanius.orchestrate.serv.enums.*;

/**
 * 公共数据
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 15:22
 */
public class CommonUtil {

    /**
     * 获取资源类型枚举数据
     *
     * @return
     *        结果信息
     */
    public static List<Map<String, Object>> getResourceType() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ResourceTypeEnum val : ResourceTypeEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", val.getCode());
            map.put("name", val.getName());
            list.add(map);
        }
        return list;
    }

    /**
     * 获取日志等级枚举数据
     *
     * @return
     *        结果信息
     */
    public static List<Map<String, Object>> getLogLevel() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (LogLevelEnum val : LogLevelEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", val.getCode());
            map.put("name", val.getName());
            list.add(map);
        }
        return list;
    }

    /**
     * 获取任务实例状态枚举数据
     *
     * @return
     *        结果信息
     */
    public static List<Map<String, Object>> getTaskInstanceStatus() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (TaskInstanceStatusEnum val : TaskInstanceStatusEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", val.getCode());
            map.put("name", val.getName());
            list.add(map);
        }
        return list;
    }

    /**
     * 获取任务编排任务实例运行中的状态
     *
     * @return
     *        状态集合
     */
    public static List<Integer> getRunTaskInstanceStatus() {
        List<Integer> runStateList =
            Arrays.asList(TaskInstanceStatusEnum.IN_SERVICE.getCode(), TaskInstanceStatusEnum.BE_INIT.getCode(),
                TaskInstanceStatusEnum.WAITING.getCode(), TaskInstanceStatusEnum.PAUSE_MANUAL.getCode());
        return runStateList;
    }

    /**
     * 获取任务编排状态枚举数据
     *
     * @return
     *        结果信息
     */
    public static List<Map<String, Object>> getTaskStatus() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (TaskStatusEnum val : TaskStatusEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", val.getCode());
            map.put("name", val.getName());
            list.add(map);
        }
        return list;
    }

    /**
     * 获取数据类型枚举数据
     *
     * @return
     *        结果信息
     */
    public static List<Map<String, Object>> getDataType() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (DataTypeEnum val : DataTypeEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", val.getCode());
            map.put("name", val.getName());
            list.add(map);
        }
        return list;
    }

    /**
     * 根据类型获取数据类型枚举名称
     *
     * @return
     *        结果信息
     */
    public static String getDataTypeNameByType(String type) {

        String dataTypeName = StringUtils.EMPTY;

        for (DataTypeEnum val : DataTypeEnum.values()) {
            if (Objects.equals(type, val.getCode())) {
                dataTypeName = val.getName();
                break;
            }
        }
        return dataTypeName;
    }

    /**
     * 根据类型获取数据库类型枚举名称
     *
     * @return
     *        结果信息
     */
    public static String getDataBaseTypeNameByType(Integer typeCode) {
        String dataBaseTypeName = StringUtils.EMPTY;

        for (DataBaseTypeEnum val : DataBaseTypeEnum.values()) {
            if (Objects.equals(typeCode, val.getCode())) {
                dataBaseTypeName = val.getValue();
                break;
            }
        }
        return dataBaseTypeName;
    }

    /**
     * 根据值获取任务实例状态code
     *
     * @return
     *        结果信息
     */
    public static Integer getTaskInstanceCodeByStatusValue(String statusValue) {
        Integer statusCode = null;

        for (TaskInstanceStatusEnum val : TaskInstanceStatusEnum.values()) {
            if (Objects.equals(statusValue, val.getValue())) {
                statusCode = val.getCode();
                break;
            }
        }
        return statusCode;
    }

}
