/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yzbdl.lanius.orchestrate.serv.dto.system.PayLoadClaimDto;
import org.yzbdl.lanius.orchestrate.serv.entity.system.ManagerEntity;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.ManagerMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.ManagerAuthService;
import org.yzbdl.lanius.orchestrate.serv.vo.system.ManagerTokenVo;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.BusinessException;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.NoPermissionException;
import org.yzbdl.lanius.orchestrate.common.jwt.JwtHelper;
import org.yzbdl.lanius.orchestrate.common.jwt.TokenDto;
import org.yzbdl.lanius.orchestrate.common.utils.BcryptEncoderUtil;

import java.util.Map;
import java.util.Optional;

/**
 * 管理员认证服务实现
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-21 14:59
 */
@Service
@Transactional
public class ManagerAuthServiceImpl extends ServiceImpl<ManagerMapper, ManagerEntity> implements ManagerAuthService {

	/**
	 * 通过用户名找到用户实体
	 * @param userName 用户名
	 * @return 用户实体对象
	 * @throws UsernameNotFoundException 框架自带的异常
	 */
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		return Optional.ofNullable(getUserByUserName(userName))
				.orElseThrow(()->new UsernameNotFoundException("用户名错误"));
	}

	/**
	 * 获取已验证的管理员用户
	 * @param userName 用户名
	 * @param password 密码
	 * @return
	 */
	public ManagerEntity getValidUser(String userName, String password){
		ManagerEntity user = (ManagerEntity) loadUserByUsername(userName);
		if(!BcryptEncoderUtil.match(password, user.getPassword())){
			throw new NoPermissionException("您的密码错误！");
		}
		return user;
	}

	/**
	 * 用户登录
	 * @param userName 用户名
	 * @param password 用户密码
	 * @return
	 */
	@Override
	public ManagerTokenVo login(String userName, String password){
		ManagerEntity managerEntity = getValidUser(userName,password);
		return getUserTokenMap(managerEntity);
	}

	/**
	 * 从管理员实体获取token视图VO
	 * @param managerEntity 管理员实体
	 * @return token视图VO
	 */
	protected ManagerTokenVo getUserTokenMap(ManagerEntity managerEntity){
		return ManagerTokenVo.builder()
				.user(managerEntity)
				.manager(true)
				.token(buildTokenFromPersonEntity(managerEntity))
				.build();
	}


	/**
	 * 通过名字获取管理员对象
	 * @param userName 用户名
	 * @return 管理员实体
	 */
	private ManagerEntity getUserByUserName(String userName) {
		return baseMapper.selectOne(
				new LambdaQueryWrapper<ManagerEntity>()
				.eq(ManagerEntity::getUsername,userName)
		);
	}

	/**
	 * 从用户实体构建token
	 * @param user 管理员实体
	 * @return token对象
	 */
	private TokenDto buildTokenFromPersonEntity(ManagerEntity user) {
		PayLoadClaimDto payLoadClaimDto = PayLoadClaimDto.builder()
				.userId(user.getId())
				.userName(user.getUsername())
				.manager(true)
				.build();
		return new TokenDto(payLoadClaimDto.translateToMap(),true);
	}

	/**
	 * 更新密码
	 * @param userId 用户id
	 * @param newPassword 新密码
	 * @param oldPassword 旧密码
	 * @return 是否成功
	 */
	@Override
	public Boolean updatePassword(Long userId, String newPassword, String oldPassword){
		ManagerEntity managerEntity = getById(userId);
		if(!BcryptEncoderUtil.match(oldPassword, managerEntity.getPassword())){
			throw new NoPermissionException("您的原始密码错误！");
		}
		return update(new LambdaUpdateWrapper<ManagerEntity>()
				.eq(ManagerEntity::getId,userId).set(ManagerEntity::getPassword, BcryptEncoderUtil.encode(newPassword))
		);
	}

	@Override
	public ManagerTokenVo refresh(String rfToken) {
		try{
			Map<String,Object> claims = JwtHelper.getManagerClaimMapFromToken(rfToken);
			PayLoadClaimDto payLoadClaimDto = JSONUtil.toBean(JSONUtil.toJsonStr(claims),PayLoadClaimDto.class);
			return getUserTokenMap(getById(payLoadClaimDto.getUserId()));
		}catch (Exception e){
			throw new BusinessException("认证失效！");
		}

	}
}
