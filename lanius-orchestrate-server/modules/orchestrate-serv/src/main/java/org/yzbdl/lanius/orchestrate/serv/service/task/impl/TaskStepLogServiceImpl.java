/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.task.impl;

import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskStepLog;
import org.yzbdl.lanius.orchestrate.serv.mapper.task.TaskStepLogMapper;
import org.yzbdl.lanius.orchestrate.serv.service.task.TaskStepLogService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskStepLogVO;

import java.util.List;

/**
 * 任务步骤日志服务实现类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-19 14:17
 */
@Service
@Slf4j
public class TaskStepLogServiceImpl extends ServiceImpl<TaskStepLogMapper, TaskStepLog> implements TaskStepLogService {

	@Override
	public List<TaskStepLogVO> getStepLogByTaskInstanceId(Long taskInstanceId) {
		return this.baseMapper.getStepLogByTaskInstanceId(taskInstanceId);
	}
}
