/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.quartz.handler.category;

import org.springframework.beans.factory.InitializingBean;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.ServerProgramInfoDTO;
import org.yzbdl.lanius.orchestrate.serv.dto.task.TaskPlanResourceDTO;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskInstance;

/**
 * 服务节点程序类型执行器
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-20 9:16
 */
public interface ProgramCategoryExecutorInvokerHandler extends InitializingBean {

    /**
     * 执行任务编排调度任务
     *
     * @param taskInstance        任务实例
     * @param taskPlanResourceDTO 任务编排
     * @param serverProgram       服务节点
     * @param isIncrementalLog    增量日志(true/false)
     */
    void executorTaskPlanJob(TaskInstance taskInstance, TaskPlanResourceDTO taskPlanResourceDTO,
                             ServerProgramInfoDTO serverProgram, Boolean isIncrementalLog);

    /**
     * 获取执行的图片
     *
     * @param taskInstanceId  任务实例ID
     * @param serverProgramId 服务节点ID
     * @return base64字符串
     */
    String transImage(Long taskInstanceId, Long serverProgramId);
}
