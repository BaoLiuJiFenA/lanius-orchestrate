/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.serv.entity.system.UserEntity;
import org.yzbdl.lanius.orchestrate.serv.vo.system.UserVo;

import java.util.List;

/**
 * 用户mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-07 14:16
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

	/**
	 * 根据组织获取用户
	 * @param orgId 组织id
	 * @param wapper mybatisplus条件
	 * @return 用户视图列表
	 */
	List<UserVo> queryUserVoByOrgId(@Param("orgId") Long orgId, @Param("skip") Long skip, @Param("pageSize") Long pageSize, @Param("ew") LambdaQueryWrapper<UserEntity> wapper);

	/**
	 * 根据组织id统计用户
	 * @param orgId 组织id
	 * @param wapper 筛选条件
	 * @return
	 */
	int countUserVoByOrgId(@Param("orgId") Long orgId,@Param("ew") LambdaQueryWrapper<UserEntity> wapper);

	/**
	 * 根据组织id查询用户列表
	 * @param orgId 组织id
	 * @param page 分页
	 * @param wapper mybatisplus条件
	 * @return 用户列表
	 */
	Page<UserEntity> queryUserByOrgId(@Param("orgId") Long orgId, IPage<UserEntity> page, @Param("ew") LambdaQueryWrapper<UserEntity> wapper);

	/**
	 * 查询第一个用户，用来判断存不存在
	 * 没有使用数据库自带的exist，减少数据库语法依赖
	 * @param orgId 组织id
	 * @return userid
	 */
	Long getFirstUserIdByOrgId(@Param("orgId") Long orgId);

}
