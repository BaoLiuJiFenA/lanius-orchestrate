/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.resource;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.ServerEntity;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.ServerProgramEntity;

/**
 * 服务节点详情
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-13 16:55
 */
@Data
public class ServerProgramInfoDTO extends ServerProgramEntity {
	@TableField(exist = false)
	private ServerEntity serverEntity;
}
