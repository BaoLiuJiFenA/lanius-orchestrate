/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.resource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourceGroupCountDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourceTypeStatisticDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.TaskResourceEntity;

import java.util.List;

/**
 * 任务资源数据操作层
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 16:49
 */
@Mapper
public interface TaskResourceMapper extends BaseMapper<TaskResourceEntity> {

	/**
	 * 获取不同资源分组计数
	 * @return 分组计数值
	 */
	@Select("select group_id, count(*) as count from lo_task_resource where deleted = false  group by group_id;")
	List<TaskResourceGroupCountDto> getResourceCount();

	/**
	 * 状态统计接口
	 * @return 状态统计
	 */
	@Select("select count(*) as count, resource_type from lo_task_resource where deleted = false group by resource_type")
	List<TaskResourceTypeStatisticDto> getStatusGroupCount();

}

