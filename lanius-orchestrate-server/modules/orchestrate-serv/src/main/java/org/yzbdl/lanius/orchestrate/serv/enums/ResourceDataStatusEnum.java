/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * 数据状态枚举
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022/3/4 16:20
 */
@Getter
@AllArgsConstructor
public enum ResourceDataStatusEnum {

    /**
     * 数据状态
     */
    ABNORMAL(0, "异常"), NORMAL(1, "正常");

    Integer code;

    String value;

    public static ResourceDataStatusEnum findValueByCode(Integer code) {
        return Optional.ofNullable(code)
            .flatMap(param -> Stream.of(ResourceDataStatusEnum.values()).filter(w -> w.code.equals(param)).findFirst())
            .orElse(null);
    }

}
