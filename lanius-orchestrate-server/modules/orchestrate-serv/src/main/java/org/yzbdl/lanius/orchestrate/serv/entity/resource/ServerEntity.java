/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.entity.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

import javax.validation.constraints.*;

/**
 * 主机配置
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("lo_server")
public class ServerEntity extends BaseOrgEntity {

    /**
    * 主机名称
    */
    @ApiModelProperty(value = "主机名称")
    @NotEmpty(message = "主机名称不能为空！")
    @Size(max = 64, message = "主机名称不能超出64位字符！")
    private String serverName;

    /**
    * 主机ip
    */
    @OprLabel
    @ApiModelProperty(value = "主机ip")
    @NotEmpty(message = "主机ip不能为空！")
    private String serverIp;

    /**
     * 主机端口
     */
    @ApiModelProperty(value = "主机端口")
    @NotNull(message = "主机ip端口不能为空！")
    @Min(value = 1, message = "主机端口请输入1-65535的正整数！")
    @Max(value = 65535, message = "主机端口请输入1-65535的正整数！")
    private Integer serverPort;

    /**
    * 账号名称
    */
    @ApiModelProperty(value = "账号名称")
    @NotEmpty(message = "账号名称不能为空！")
    private String accountName;

    /**
    * 密码
    */
    @NotEmpty(message = "账号密码不能为空！")
    @ApiModelProperty(value = "账号密码")
    private String password;

    /**
    * 备注
    */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 状态 1 - 正常，0 - 异常
    */
    @ApiModelProperty(value = "状态 1 - 正常，0 - 异常")
    private Integer status;

    /**
    * 是否被删除
    */
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;

}

