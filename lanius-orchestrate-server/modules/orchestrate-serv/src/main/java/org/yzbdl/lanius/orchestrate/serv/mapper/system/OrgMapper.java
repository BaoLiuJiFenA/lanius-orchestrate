/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.yzbdl.lanius.orchestrate.serv.entity.system.OrgEntity;

import java.util.List;

/**
 * 组织 Mapper
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 13:29
 */
@Mapper
public interface OrgMapper extends BaseMapper<OrgEntity> {


	/**
	 * 查看用户关联的组织
	 * 改组织未冻结
	 * @param userId 用户id
	 * @return 组织列表
	 */
	List<OrgEntity> queryOrgByUserId(@Param("userId") Long userId);
}
