package org.yzbdl.lanius.orchestrate.serv.entity.task;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.yzbdl.lanius.orchestrate.serv.enums.LogLevelEnum;
import org.yzbdl.lanius.orchestrate.serv.enums.TaskStatusEnum;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLabel;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.cron.CronValid;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.enums.EnumValid;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseOrgEntity;

/**
 * 任务编排表
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-06 16:55
 */
@Data
@TableName(value = "lo_task_plan")
public class
TaskPlan extends BaseOrgEntity {

    @OprLabel
    @NotBlank(message = "任务名称不能为空")
    @Size(min = 0, max = 64, message = "任务名称字符长度不能超过64")
    @ApiModelProperty(value = "任务名称")
    @TableField(value = "task_name")
    private String taskName;

    @NotBlank(message = "调度cron不能为空")
    @Size(min = 0, max = 512, message = "任务名称字符长度不能超过512")
    @CronValid(message = "Cron表达式格式不正确或者配置时间不合法")
    @ApiModelProperty(value = "调度cron")
    @TableField(value = "task_cron")
    private String taskCron;

    @NotNull(message = "服务节点不能为空")
    @ApiModelProperty(value = "服务节点id")
    @TableField(value = "server_program_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long serverProgramId;

    @NotNull(message = "任务资源不能为空")
    @ApiModelProperty(value = "任务资源id")
    @TableField(value = "task_resource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taskResourceId;

    @ApiModelProperty(value = "备注")
    @TableField(value = "remark")
    private String remark;


    @ApiModelProperty(value = "任务配置")
    @TableField(value = "json_config")
    private String jsonConfig;

    @NotNull(message = "状态不能为空")
    @EnumValid(target = TaskStatusEnum.class,message = "状态编码格式不正确")
    @ApiModelProperty(value = "1-停止，2-运行中")
    @TableField(value = "status")
    private Integer status;

    @NotNull(message = "分组不能为空")
    @ApiModelProperty(value = "分组id")
    @TableField(value = "group_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;

    @EnumValid(target = LogLevelEnum.class,message = "日志等级编码格式不正确")
    @NotNull(message = "日志等级不能为空")
    @ApiModelProperty(value = "日志等级")
    @TableField(value = "log_level")
    private Integer logLevel;


}
