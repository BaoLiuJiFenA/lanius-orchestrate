/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.utils;

import java.net.URI;

/**
 * Jdbc连接信息工具
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-14 15:39
 */
public class JdbcUtil {


	/**
	 * 解析URI
	 *
	 * @param jdbcUrl
	 *        jdbc连接url
	 * @return
	 *        URI
	 */
	public static URI parseJdbcUrl(String jdbcUrl){
		String cleanUri = jdbcUrl.substring(5);
		URI uri = URI.create(cleanUri);
		return uri;
	}

	/**
	 * 根据uri进行截取字符串获取数据库名称
	 *
	 * @param uri
	 *        uri
	 * @return
	 *        数据库名称
	 */
	public static String getDbName(URI uri){
		String path = uri.getPath();
		path = path.substring(1, path.length());
		String[] sb = path.split("/");
		return sb[0];
	}

}
