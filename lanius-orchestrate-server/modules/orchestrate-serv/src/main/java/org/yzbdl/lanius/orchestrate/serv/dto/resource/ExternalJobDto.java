package org.yzbdl.lanius.orchestrate.serv.dto.resource;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 外部作业实体类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-13 15:49:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("R_JOB")
public class ExternalJobDto{

    /**
     * id
     */
    private Long idJob;

    private Long idDirectory;

    private String name;

    private String description;

    private String extendedDescription;

    private String jobVersion;

    private Long jobStatus;

    private Long idDatabaseLog;

    private String tableNameLog;

    private String createdUser;

    private LocalDateTime createdDate;

    private String modifiedUser;

    private LocalDateTime modifiedDate;

    private String useBatchId;

    private String passBatchId;

    private String useLogfield;

    private String sharedFile;
    
}

