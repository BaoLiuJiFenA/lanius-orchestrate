/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.task;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.yzbdl.lanius.orchestrate.serv.dto.task.TaskInstanceQueryDTO;
import org.yzbdl.lanius.orchestrate.serv.dto.task.TaskInstanceResourceDTO;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskInstance;
import org.yzbdl.lanius.orchestrate.serv.vo.task.ScheduleTaskInstanceVO;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskInstanceVO;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 任务实例Mapper
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 9:05
 */
@Mapper
public interface TaskInstanceMapper extends BaseMapper<TaskInstance> {

    /**
     * 分页查询任务实例列表
     *
     * @param page
     *        分页信息
     * @param instanceQueryDTO
     *        查询条件
     * @param pageSize
     *        每页条数
     * @param startIndex
     *        起始索引
     * @return
     *        结果信息
     */
    IPage<TaskInstanceVO> queryPage(@Param("page") Page<TaskInstanceVO> page,
        @Param("instanceQueryDTO") TaskInstanceQueryDTO instanceQueryDTO,@Param("startIndex") long startIndex,@Param("pageSize") long pageSize);

    /**
     * 获取调度总览之任务实例
     *
     * @return
     *        结果信息
     */
    List<ScheduleTaskInstanceVO> getSchedulerViewStatistics();

    /**
     * 获取任务实例信息，忽略租户
     *
     * @param taskInstanceId
     *        任务实例ID
     * @return
     *        任务实例
     */
    @InterceptorIgnore(tenantLine = "on")
    TaskInstance getByIdIgnoreTenantId(@Param("taskInstanceId") Long taskInstanceId);

    /**
     * 判断是否存在运行的任务示例
     * @param taskResourceId 任务资源id
     * @return 任务实例
     */
    TaskInstance getRunningTaskInstance(@Param("taskResourceId") Long taskResourceId);

    /**
     * 获取一条运行中的实例，根据任务编排ID
     *
     * @param runStateList
     *        运行状态
     * @param taskPlanId
     *        任务编排ID
     * @return
     *        任务实例
     */
    @InterceptorIgnore(tenantLine = "on")
    TaskInstance getOneInstanceForRunIgnoreTenantId(@Param("runStateList") List<Integer> runStateList,
        @Param("taskPlanId") Long taskPlanId);

    /**
     * 根据id更新实例，忽略租户ID
     *
     * @param taskInstance
     *        实例
     * @return
     *        影响条数
     */
    @InterceptorIgnore(tenantLine = "on")
    @Update(" update lo_task_instance set status = #{status},end_time = #{endTime} where id = #{id}")
    int updateByIdIgnoreTenantId(TaskInstance taskInstance);

    /**
     * 获取任务实例资源信息
     *
     * @param taskInstanceId
     *        任务实例ID
     * @return
     *        任务实例资源信息
     */
    @InterceptorIgnore(tenantLine = "on")
    TaskInstanceResourceDTO getTaskInstanceResourceIgnoreTenantId(@Param("taskInstanceId") Long taskInstanceId);
}
