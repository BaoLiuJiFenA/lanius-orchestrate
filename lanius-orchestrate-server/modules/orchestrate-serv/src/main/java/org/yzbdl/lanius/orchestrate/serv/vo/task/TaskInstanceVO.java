/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.vo.task;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskInstance;

/**
 * 任务实例分页查询响应实体类
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 9:21
 */
@Data
public class TaskInstanceVO extends TaskInstance {

	@TableField(exist = false)
	@ApiModelProperty(value = "任务名称")
	private String taskName;

	@TableField(exist = false)
	@ApiModelProperty(value = "日志等级描述")
	private String logLevelDes;

	@TableField(exist = false)
	@ApiModelProperty(value = "任务资源名称")
	private String taskResourceName;

	@TableField(exist = false)
	@ApiModelProperty(value = "任务资源类型")
	private String taskResourceType;

	@TableField(exist = false)
	@ApiModelProperty(value = "任务资源类型描述")
	private String taskResourceTypeDes;

	@TableField(exist = false)
	@ApiModelProperty(value = "服务节点名称")
	private String serverProgramName;

	@TableField(exist = false)
	@ApiModelProperty(value = "状态描述")
	private String statusDes;

	@TableField(exist = false)
	@ApiModelProperty(value = "时间差")
	private String diff;

}
