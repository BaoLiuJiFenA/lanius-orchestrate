/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.mapper.resource;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.StatusStatisticDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.TaskResourceConfigEntity;

import java.util.List;

/**
 * 任务资源配置数据操作层
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-08 16:14
 */
@Mapper
public interface TaskResourceConfigMapper extends BaseMapper<TaskResourceConfigEntity> {

	/**
	 * 获取所有任务资源配置不区分主租户
	 * @return list任务资源配置
	 */
	@InterceptorIgnore(tenantLine = "on")
	@Select("select * from lo_task_resource_config where deleted = false")
	List<TaskResourceConfigEntity> getAllTaskResource();

	/**
	 * 状态统计接口
	 * @return 状态统计
	 */
	@Select("select count(*) as count, status from lo_task_resource_config where deleted = false group by status")
	List<StatusStatisticDto> getStatusGroupCount();

	/**
	 * 批量更新任务资源配置状态
	 * @param list list配置数据
	 */
	@InterceptorIgnore(tenantLine = "on")
	void batchUpdate(List<TaskResourceConfigEntity> list);

}

