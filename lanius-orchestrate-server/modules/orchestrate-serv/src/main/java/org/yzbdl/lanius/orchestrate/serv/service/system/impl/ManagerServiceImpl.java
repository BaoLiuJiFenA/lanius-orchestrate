/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.yzbdl.lanius.orchestrate.serv.dto.system.ManagerInsertParamDto;
import org.yzbdl.lanius.orchestrate.serv.dto.system.ManagerListDto;
import org.yzbdl.lanius.orchestrate.serv.entity.system.ManagerEntity;
import org.yzbdl.lanius.orchestrate.serv.mapper.system.ManagerMapper;
import org.yzbdl.lanius.orchestrate.serv.service.system.ManagerService;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.BusinessException;
import org.yzbdl.lanius.orchestrate.common.utils.BcryptEncoderUtil;

import java.time.LocalDateTime;

/**
 * 管理员服务实现
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-08 11:00
 */
@Service
public class ManagerServiceImpl extends ServiceImpl<ManagerMapper, ManagerEntity> implements ManagerService {

    /**
     * 通过名称获取人员信息
     * @param userName 用户名称
     * @return 管理员实体
     */
    private ManagerEntity getUserByUserName(String userName) {
        return baseMapper.selectOne(new LambdaQueryWrapper<ManagerEntity>()
                .eq(ManagerEntity::getUsername,userName)
        );
    }


    /**
     * 分页查询管理员列表
     * @param managerListDto 管理员查询条件
     * @param page 分页
     * @return 管理员列表
     */
    @Override
    public Page<ManagerEntity> listManagerPage(ManagerListDto managerListDto, Page<ManagerEntity> page){
        return baseMapper.selectPage(
                page,
                new QueryWrapper<ManagerEntity>().lambda()
                        .like(StringUtils.isNotEmpty(managerListDto.getUserName()),ManagerEntity::getUsername,managerListDto.getUserName())
        );
    }

    /**
     * 新增管理员
     * @param managerInsertParamDto 管理员新增参数类
     * @return 成功与否
     */
    @Override
    public boolean insertManager(ManagerInsertParamDto managerInsertParamDto){
        ManagerEntity existManager = getUserByUserName(managerInsertParamDto.getUserName());
        if(null!=existManager){
            throw new BusinessException("重复的用户名！");
        }
        ManagerEntity managerEntity = ManagerEntity.builder()
                .userName(managerInsertParamDto.getUserName())
                .createTime(LocalDateTime.now())
                .password(BcryptEncoderUtil.encode(managerInsertParamDto.getPassword()))
                .build();
        return save(managerEntity);
    }


}
