/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.system;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.yzbdl.lanius.orchestrate.common.base.dto.TreeNodeDto;

/**
 * 菜单树形图
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-12 16:08
 */
@Data
@SuperBuilder
public class MenuTreeDto extends TreeNodeDto {
	private String menuName;
	private String url;
}
