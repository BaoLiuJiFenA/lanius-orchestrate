/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.serv.dto.system;

import lombok.Data;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.password.ComplexValid;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 组织用户参数dto
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-25 10:27
 */
@Data
public class OrgUserParamDto {
	@NotBlank(message = "组织名称不能为空")
	@Size(max = 64)
	private String orgName;
	@NotBlank(message = "用户名不能为空")
	@Size(max = 64)
	private String userName;
	@NotBlank(message = "用户名昵称为空")
	@Size(max = 64)
	private String nickName;
	@NotBlank(message = "用户密码不能为空")
	@Size(max = 32, min = 6)
	@ComplexValid(message = "密码必须包含英文大小写，数字，特殊字符！")
	private String password;
}
