package org.yzbdl.lanius.orchestrate.serv.dto.resource;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yzbdl.lanius.orchestrate.serv.enums.ResourceTypeEnum;
import org.yzbdl.lanius.orchestrate.common.base.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * (TaskResource)表实体类
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @since 2022-04-07 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class TaskResourcePageDto extends BaseEntity {
        
    /**
    * 资源类型  1作业 2转换
    * @see ResourceTypeEnum
    */    
    @ApiModelProperty(value = "资源类型 1作业 2转换")
    private Integer resourceType;
        
    /**
    * 备注
    */    
    @ApiModelProperty(value = "备注")
    private String remark;
        
    /**
    * 资源配置
    */    
    @ApiModelProperty(value = "资源配置")
    private String resourceContent;

    /**
     * 资源配置id
     */
    @ApiModelProperty(value = "资源配置id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceConfigId;
                        
    /**
    * 资源组id
    */    
    @ApiModelProperty(value = "资源组id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
        
    /**
    * 资源名称
    */    
    @ApiModelProperty(value = "资源名称")
    private String resourceName;
        
    /**
    * 是否被删除
    */    
    @ApiModelProperty(value = "是否被删除")
    private Boolean deleted;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

}

