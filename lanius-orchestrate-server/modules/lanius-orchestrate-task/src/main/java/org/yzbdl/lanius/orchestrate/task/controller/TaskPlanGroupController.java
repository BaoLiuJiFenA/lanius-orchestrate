package org.yzbdl.lanius.orchestrate.task.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.entity.task.TaskPlanGroup;
import org.yzbdl.lanius.orchestrate.serv.service.task.TaskPlanGroupService;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskPlanGroupVO;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.base.controller.BaseController;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.exception.runtime.BusinessException;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.yzbdl.lanius.orchestrate.common.utils.MessageUtil;

/**
 * 任务编排相关分组接口控制器
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-07 16:14
 */
@Slf4j
@Api(value = "任务编排相关分组接口控制器", tags = "任务编排相关分组接口控制器")
@RestController
@RequestMapping("/task/plan/group")
public class TaskPlanGroupController extends BaseController {

    @Autowired
    private TaskPlanGroupService taskPlanGroupService;

    /**
     * 分组树形结构并统计任务编排数量
     *
     * @return
     *        树形信息
     */
    @CheckPermission("task::plan::query")
    @GetMapping(value = "/tree/list")
    @ApiOperation(value = "任务编排分组树形结构", notes = "任务编排分组树形结构")
    @ResponseBody
    public ResultObj<List<TaskPlanGroupVO>> treeList() {
        return ResultObj.success(taskPlanGroupService.treeList());
    }

    /**
     * 新增根节点分组
     *
     * @param taskPlanGroup
     *        任务编排分组
     * @return
     *        结果信息
     */
    @OprLog(value = UserOprEventConstant.TASK_PLAN_GROUP_ADD,msg="新增任务计划分组<${groupName}>")
    @CheckPermission("task::plan::edit")
    @PostMapping(value = "/rootNode")
    @ApiOperation(value = "新增根节点分组", notes = "新增根节点分组")
    @ResponseBody
    public ResultObj<Boolean> saveRootEntity(@RequestBody @Validated TaskPlanGroup taskPlanGroup) {
        taskPlanGroup.setPid(null);
        return ResultObj.success(taskPlanGroupService.saveEntity(taskPlanGroup));
    }

    /**
     * 新增子节点分组
     *
     * @param taskPlanGroup
     *        任务编排分组
     * @return
     *        结果信息
     */
    @OprLog(value = UserOprEventConstant.TASK_PLAN_GROUP_ADD,msg="新增任务计划分组子节点<${groupName}>")
    @CheckPermission("task::plan::edit")
    @PostMapping(value = "/childNode")
    @ApiOperation(value = "新增子节点分组", notes = "新增子节点分组")
    @ResponseBody
    public ResultObj<Boolean> saveChildEntity(@RequestBody @Validated TaskPlanGroup taskPlanGroup) {
        if (Objects.isNull(taskPlanGroup.getPid())) {
            throw new BusinessException(MessageUtil.get("task.group.add_child"));
        }
        return ResultObj.success(taskPlanGroupService.saveEntity(taskPlanGroup));
    }

    /**
     * 编辑分组
     *
     * @param taskPlanGroup
     *        任务编排分组
     * @return
     *        结果信息
     */
    @OprLog(value = UserOprEventConstant.TASK_PLAN_GROUP_UPDATE,msg="修改分组<${groupName}>")
    @CheckPermission("task::plan::edit")
    @PutMapping(value = "/taskPlanGroup")
    @ApiOperation(value = "编辑分组", notes = "编辑分组")
    @ResponseBody
    public ResultObj<Boolean> updateEntity(@RequestBody @Validated TaskPlanGroup taskPlanGroup) {
        return ResultObj.success(taskPlanGroupService.updateEntity(taskPlanGroup));
    }

    /**
     * 批量更新分组
     *
     * @param taskPlanGroups
     *        任务编排分组
     * @return
     *        结果信息
     */
    @OprLog(value = UserOprEventConstant.TASK_PLAN_GROUP_UPDATE,msg="批量修改分组")
    @CheckPermission("task::plan::edit")
    @PutMapping(value = "/batch/taskPlanGroup")
    @ApiOperation(value = "批量编辑分组", notes = "批量编辑分组")
    @ResponseBody
    public ResultObj<Boolean> batchUpdateEntity(@RequestBody @Validated List<TaskPlanGroup> taskPlanGroups) {
        return ResultObj.success(taskPlanGroupService.batchUpdateEntity(taskPlanGroups));
    }

    /**
     * 根据id删除分组信息
     *
     * @param id
     *        分组ID
     * @return
     *        结果信息
     */
    @OprLog(value = UserOprEventConstant.TASK_PLAN_GROUP_DELETE,msg="删除分组<${groupName}>,id是<${id}>")
    @CheckPermission("task::plan::delete")
    @DeleteMapping(value = "/taskPlanGroup/{id}")
    @ApiOperation(value = "根据ID删除分组", notes = "根据ID删除分组")
    @ResponseBody
    public ResultObj<Boolean> deleteEntity(@PathVariable(value = "id", required = true) @OprId(server = TaskPlanGroupService.class) Long id) {
        return ResultObj.success(taskPlanGroupService.deleteEntity(id));
    }

    /**
     * 校验分组名称是否重复
     *
     * @param taskPlanGroup
     *        分组实体参数
     * @return
     *        结果信息
     */
    @CheckPermission("task::plan::query")
    @PostMapping(value = "/repeatGroup")
    @ApiOperation(
        value = "校验分组名称是否重复（true/false）", notes = "校验分组名称是否重复")
    @ResponseBody
    public ResultObj<Boolean> checkRepeatGroup(@RequestBody @Validated TaskPlanGroup taskPlanGroup) {
        return ResultObj.success(taskPlanGroupService.checkRepeatGroup(taskPlanGroup));
    }

}
