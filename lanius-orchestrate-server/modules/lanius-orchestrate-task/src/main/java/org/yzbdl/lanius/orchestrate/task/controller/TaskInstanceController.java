/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.task.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.dto.task.IncrLogQueryDTO;
import org.yzbdl.lanius.orchestrate.serv.dto.task.TaskInstanceQueryDTO;
import org.yzbdl.lanius.orchestrate.serv.service.task.TaskInstanceService;
import org.yzbdl.lanius.orchestrate.serv.service.task.TaskStepLogService;
import org.yzbdl.lanius.orchestrate.serv.vo.task.IncrLogVO;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskInstanceVO;
import org.yzbdl.lanius.orchestrate.serv.vo.task.TaskStepLogVO;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.base.controller.BaseController;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 任务实例控制管理相关接口
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-11 9:10
 */
@Slf4j
@Api(value = "任务实例控制管理相关接口", tags = "任务实例控制管理相关接口")
@RestController
@RequestMapping("/task/instance/")
public class TaskInstanceController extends BaseController {

    @Autowired
    private TaskInstanceService taskInstanceService;

    @Autowired
    private TaskStepLogService taskStepLogService;

    /**
     * 任务实例分页列表
     *
     * @param page                 页码
     * @param size                 每页数量
     * @param taskInstanceQueryDTO 查询参数
     * @return 结果信息
     */
    @CheckPermission("task::plan::instance::query")
    @PostMapping(value = "queryPage")
    @ApiOperation(value = "分页列表", notes = "任务实例分页列表")
    @ResponseBody
    public ResultObj<IPage<TaskInstanceVO>> queryPage(@RequestParam(defaultValue = "1") Integer page,
                                                      @RequestParam(defaultValue = "20") Integer size, @RequestBody TaskInstanceQueryDTO taskInstanceQueryDTO) {
        IPage<TaskInstanceVO> iPage = taskInstanceService.queryPage(buildIPage(page, size), taskInstanceQueryDTO);
        return ResultObj.success(iPage);
    }

    /**
     * 获取图片
     *
     * @param taskInstanceId  任务实例ID
     * @param serverProgramId 服务节点ID
     * @return base64字符串
     */
    @CheckPermission("task::plan::instance::query")
    @GetMapping(value = "transImage/{taskInstanceId}/{serverProgramId}")
    @ApiOperation(value = "图片", notes = "图片")
    @ResponseBody
    public ResultObj<String> getTransImageById(@PathVariable(value = "taskInstanceId") Long taskInstanceId,
                                               @PathVariable(value = "serverProgramId") Long serverProgramId) {
        return ResultObj.success(taskInstanceService.getTransImageById(taskInstanceId, serverProgramId));
    }


    /**
     * 获取步骤度量
     *
     * @param taskInstanceId 任务实例ID
     * @return 结果数据
     */
    @CheckPermission("task::plan::instance::query")
    @GetMapping(value = "stepLog/{taskInstanceId}")
    @ApiOperation(value = "获取步骤度量", notes = "获取步骤度量")
    @ResponseBody
    public ResultObj<List<TaskStepLogVO>> getStepLogByTaskInstanceId(@PathVariable(value = "taskInstanceId") Long taskInstanceId) {
        return ResultObj.success(taskStepLogService.getStepLogByTaskInstanceId(taskInstanceId));
    }


    /**
     * 读取任务增量日志
     *
     * @param response        response
     * @param page            页码
     * @param size            行数
     * @param incrLogQueryDTO 查询参数
     * @return 文本指针
     */
    @CheckPermission("task::plan::instance::query")
    @PostMapping(value = "incrLog")
    @ApiOperation(value = "读取任务实例日志内容", notes = "读取任务实例日志内容")
    @ResponseBody
    public ResultObj<IncrLogVO> readIncrLog(HttpServletResponse response, @RequestParam(defaultValue = "0") Long page,
                                            @RequestParam(defaultValue = "100") Long size, @RequestBody @Validated IncrLogQueryDTO incrLogQueryDTO) {
        return ResultObj.success(taskInstanceService.readIncrLog(response, page, size, incrLogQueryDTO));
    }
}
