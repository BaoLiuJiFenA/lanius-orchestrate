/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 特殊字符工具
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-06-08 12:54
 */
public class SpecialCharacterUtil {

	public static String escapeStr(String str){
		if(StringUtils.isNotBlank(str)){
			str = str.replaceAll("\\\\","\\\\\\\\");
			str = str.replaceAll("_","\\\\_");
			str = str.replaceAll("%","\\\\%");
		}
		return str;
	}
}
