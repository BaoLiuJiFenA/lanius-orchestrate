/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.xml;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * XML解析器构建工厂
 *
 * @author jinchunzhao@yzbdl.ac.cn
 * @date 2022-04-26 14:30
 */
public class XMLParserFactoryProducer {
    private static final Log logger = LogFactory.getLog(XMLParserFactoryProducer.class);

    public XMLParserFactoryProducer() {}

    /**
     * 创建DocumentBuilderFactory
     *
     * @return
     *        DocumentBuilderFactory
     * @throws ParserConfigurationException
     *         ParserConfigurationException
     */
    public static DocumentBuilderFactory createSecureDocBuilderFactory() throws ParserConfigurationException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        docBuilderFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
        docBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        return docBuilderFactory;
    }
}
