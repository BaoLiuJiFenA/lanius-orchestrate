/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.annotation.valid.password;

import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.support.CronExpression;
import org.yzbdl.lanius.orchestrate.common.annotation.valid.enums.EnumValid;
import org.yzbdl.lanius.orchestrate.common.utils.PasswordComplexUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * 复杂度校验处理器
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-25 09:10
 */
public class ComplexValidator implements ConstraintValidator<ComplexValid,Object> {

	@Override
	public void initialize(ComplexValid constraintAnnotation) {
		/**
		 * 复杂度校验注解
		 */
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
		return PasswordComplexUtil.check(value.toString());
	}
}
