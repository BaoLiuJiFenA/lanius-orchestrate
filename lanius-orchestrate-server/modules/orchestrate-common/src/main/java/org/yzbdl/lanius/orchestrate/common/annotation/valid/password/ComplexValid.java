/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.common.annotation.valid.password;

import org.yzbdl.lanius.orchestrate.common.annotation.valid.cron.CronValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 复杂度校验
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-24 18:29
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {ComplexValidator.class})
public @interface ComplexValid {

	/**
	 * 提示消息
	 *
	 * @return
	 *        消息
	 */
	String message() default "";

	/**
	 * 作用参考@Validated和@Valid的区别
	 *
	 * @return
	 *        分组
	 */
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	/**
	 * 目标枚举类
	 * @return
	 *       目标枚举类
	 */
	Class<?> target() default Class.class;

	/**
	 * 是否忽略空值
	 *
	 * @return
	 *        true/false
	 */
	boolean ignoreEmpty() default true;
}
