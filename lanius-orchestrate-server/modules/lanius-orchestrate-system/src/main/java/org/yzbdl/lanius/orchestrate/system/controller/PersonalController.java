/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.dto.system.UpdatePasswordDto;
import org.yzbdl.lanius.orchestrate.serv.service.system.*;
import org.yzbdl.lanius.orchestrate.serv.utils.CurrentUserUtil;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import java.util.ArrayList;
import java.util.List;

/**
 * 个人接口，不用鉴权
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-12 15:01
 */
@Api(tags = "用户个人接口")
@RestController
@RequestMapping("/personal")
public class PersonalController {


	@Autowired
	MenuService menuService;
	@Autowired
	PermissionService permissionService;
	@Autowired
	UserService userService;
	@Autowired
	UserAuthService userAuthService;
	@Autowired
	OrgService orgService;

	@ApiOperation(value="获取个人权限", notes="")
	@RequestMapping(value = "/permission", method = RequestMethod.GET)
	public ResultObj getPermission(){
		if(CurrentUserUtil.isOrgChief()){
			return ResultObj.success(new ArrayList<>());
		}
		return ResultObj.success(permissionService.queryPermissionsByUserId(CurrentUserUtil.getCurrentUserId()));
	}

	@ApiOperation(value="获取用户昵称", notes="")
	@RequestMapping(value = "/nicknames", method = RequestMethod.POST)
	public ResultObj getNickNamesByIds(@RequestBody List<Long> ids){
		return ResultObj.success(userService.listNickNamesByIds(ids));
	}


	@ApiOperation(value="更换组织架构", notes="")
	@RequestMapping(value = "/org/change/{orgId}", method = RequestMethod.GET)
	public ResultObj changeOrgId(@PathVariable("orgId") Long orgId){
		if(orgService.isCurrentUserBelongToOrg(orgId)){
			return ResultObj.success(userAuthService.refreshTokenByOrgId(orgId));
		}
		return ResultObj.error("该用户未关联该组织");
	}

	@ApiOperation(value="获取所有相关组织", notes="")
	@RequestMapping(value = "/org", method = RequestMethod.GET)
	public ResultObj listOrg(){
		return ResultObj.success(orgService.queryOrgByUserId(CurrentUserUtil.getCurrentUserId()));
	}

	@CheckPermission("user::resetPassword")
	@ApiOperation(value="更改用户密码", notes="")
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	public ResultObj updatePassword(@RequestBody UpdatePasswordDto updatePasswordDto){
		return ResultObj.success(userAuthService.updatePassword(
				CurrentUserUtil.getCurrentUserId(),
				updatePasswordDto.getNewPassword(),
				updatePasswordDto.getOldPassword()
		));
	}



}
