/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.system.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yzbdl.lanius.orchestrate.serv.service.system.MenuService;

/**
 * 菜单管理
 *
 * @author chenjunhao@yzbdl.ac.cn
 * @date 2022-04-11 11:17
 */
@Api(tags = "菜单管理接口")
@RestController
@RequestMapping("/menu")
public class MenuController {


	@Autowired
	MenuService menuService;



//	@ApiOperation(value="添加菜单", notes="")
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	public ResultObj insert(@RequestBody MenuEntity menuEntity){
//		return ResultObj.success(menuService.saveMenu(menuEntity));
//	}
//
//	@ApiOperation(value="修改菜单", notes="")
//	@RequestMapping(value = "", method = RequestMethod.PUT)
//	public ResultObj update(@RequestBody MenuEntity menuEntity){
//		return ResultObj.success(menuService.updateMenu(menuEntity));
//	}

//	@CheckPermission("role::query")
//	@ApiOperation(value="获取用户菜单菜单", notes="")
//	@RequestMapping(value = "", method = RequestMethod.GET)
//	public ResultObj tree(){
//		return ResultObj.success(menuService.li());
//	}









}
