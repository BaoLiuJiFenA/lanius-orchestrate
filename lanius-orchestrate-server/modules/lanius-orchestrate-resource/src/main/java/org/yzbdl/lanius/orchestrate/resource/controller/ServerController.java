/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.ServerPageDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.ServerEntity;
import org.yzbdl.lanius.orchestrate.serv.service.resource.ServerService;
import org.yzbdl.lanius.orchestrate.serv.utils.SshUtils;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.utils.ExceptionUtil;

import java.util.Objects;

/**
 * 服务器模块接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-03-29 14:46
 */
@Api(tags = "服务器模块接口")
@RequestMapping("/resource/server")
@RestController
@AllArgsConstructor
public class ServerController {

    private final ServerService serverService;

    /**
     * 添加服务器数据
     * @param serverEntity 待添加服务器数据
     * @return true|false
     */
    @OprLog(value = UserOprEventConstant.SERVER_ADD,msg="添加ip:<${serverIp}>服务器数据")
    @CheckPermission("resource::server::edit")
    @ApiOperation(value = "添加服务器")
    @PostMapping()
    public ResultObj<Boolean> add(@RequestBody @Validated ServerEntity serverEntity) {
        return ResultObj.success(serverService.addServer(serverEntity));
    }

    /**
     * 逻辑删除服务器数据
     * @param id id
     * @return true|false
     */
    @OprLog(value = UserOprEventConstant.SERVER_DELETE,msg="删除ip:<${serverIp}>服务器数据")
    @CheckPermission("resource::server::edit")
    @ApiOperation(value = "逻辑删除服务器")
    @DeleteMapping("/{id}")
    public ResultObj<Boolean> delete(@PathVariable @OprId(server = ServerService.class) Long id) {
        return ResultObj.success(serverService.deleteServer(id));
    }

    /**
     * 根据id获取服务器数据
     * @param id id
     * @return 根据id获取服务器数据
     */
    @CheckPermission("resource::server::query")
    @ApiOperation(value = "获取任务")
    @GetMapping()
    public ResultObj<ServerEntity> getServer(@RequestParam Long id) {
        return ResultObj.success(serverService.getServer(id));
    }

    /**
     * 分页查询服务器数据
     * @param serverPageDto serverPageDto 分页参数
     * @return 分页数据
     */
    @CheckPermission("resource::server::query")
    @ApiOperation(value = "分页查看服务器")
    @PostMapping("query")
    public ResultObj<IPage<ServerEntity>> pageServer(@RequestParam(defaultValue = "1") Integer page,
                                                     @RequestParam(defaultValue = "20") Integer size,
                                                     @RequestBody ServerPageDto serverPageDto) {
        return ResultObj.success(serverService.pageServer(page, size, serverPageDto));
    }

    /**
     * 更新服务器数据
     * @param serverEntity 待更新服务器参数
     * @return true|false
     */
    @OprLog(value = UserOprEventConstant.SERVER_UPDATE,msg="更新ip:<${serverIp}>服务器数据")
    @CheckPermission("resource::server::edit")
    @ApiOperation(value = "更新服务器数据")
    @PutMapping()
    public ResultObj<Boolean> updateServer(@RequestBody @Validated ServerEntity serverEntity) {
        return ResultObj.success(serverService.updateServer(serverEntity));
    }

    /**
     * 测试服务器连通性
     */
    @CheckPermission("resource::server::query")
    @ApiOperation(value = "测试服务器连通性")
    @PostMapping("/testConnection")
    public ResultObj<Boolean> testConnection(@RequestBody ServerEntity serverEntity) {
        ExceptionUtil.checkParam(StringUtils.hasLength(serverEntity.getServerIp())
            && Objects.nonNull(serverEntity.getServerPort()) && StringUtils.hasLength(serverEntity.getAccountName())
            && StringUtils.hasLength(serverEntity.getPassword()), "请确保服务器ip、端口、账户、密码完全输入！");
        return ResultObj.success(SshUtils.serverTestConnection(serverEntity.getServerIp(), serverEntity.getServerPort(),
            serverEntity.getAccountName(), serverEntity.getPassword()));
    }

}
