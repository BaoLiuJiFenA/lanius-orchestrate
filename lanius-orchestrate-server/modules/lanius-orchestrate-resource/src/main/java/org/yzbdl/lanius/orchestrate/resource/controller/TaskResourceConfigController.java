/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.dataBase.DynamicDataBase;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourceConfigPageDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.TaskResourceConfigEntity;
import org.yzbdl.lanius.orchestrate.serv.enums.DataBaseTypeEnum;
import org.yzbdl.lanius.orchestrate.serv.service.resource.TaskResourceConfigService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;
import org.yzbdl.lanius.orchestrate.common.utils.ExceptionUtil;

/**
 * 任务资源配置模块接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-08 16:14
 */
@Api(tags = "任务资源配置模块接口")
@RequestMapping("/resource/taskResourceConfig")
@RestController
@AllArgsConstructor
public class TaskResourceConfigController {

	private final TaskResourceConfigService taskResourceConfigService;

	/**
	 * 添加任务资源配置数据
	 * @param taskResourceConfigEntity 待添加任务资源配置数据
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_CONFIG_ADD,msg="添加<${connectName}>任务资源配置数据")
	@CheckPermission("resource::taskResourceConfig::edit")
	@ApiOperation(value = "添加任务资源配置")
	@PostMapping()
	public ResultObj<String> add(@RequestBody @Validated TaskResourceConfigEntity taskResourceConfigEntity) {
		taskResourceConfigService.addTaskResourceConfig(taskResourceConfigEntity);
		return ResultObj.success();
	}

	/**
	 * 逻辑删除任务资源配置数据
	 * @param id id
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_CONFIG_DELETE,msg="删除<${connectName}>任务资源配置数据")
	@CheckPermission("resource::taskResourceConfig::edit")
	@ApiOperation(value = "逻辑删除任务资源配置")
	@DeleteMapping("/{id}")
	public ResultObj<Boolean> delete(@PathVariable @OprId(server = TaskResourceConfigService.class) Long id) {
		return ResultObj.success(taskResourceConfigService.deleteTaskResourceConfig(id));
	}

	/**
	 * 根据id获取任务资源配置数据
	 * @param id id
	 * @return 根据id获取任务资源配置数据
	 */
	@CheckPermission("resource::taskResourceConfig::query")
	@ApiOperation(value = "获取任务")
	@GetMapping()
	public ResultObj<TaskResourceConfigEntity> getTaskResourceConfig(@RequestParam Long id) {
		return ResultObj.success(taskResourceConfigService.getTaskResourceConfig(id));
	}

    /**
     * 分页查询任务资源配置数据
     * @param taskResourceConfigPageDto taskResourceConfigPageDto 分页参数
     * @return 分页数据
     */
    @CheckPermission("resource::taskResourceConfig::query")
    @ApiOperation(value = "分页查看任务资源配置")
    @PostMapping("query")
    public ResultObj<IPage<TaskResourceConfigEntity>> pageTaskResourceConfig(
        @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "20") Integer size,
        @RequestBody TaskResourceConfigPageDto taskResourceConfigPageDto) {
        return ResultObj.success(taskResourceConfigService.pageTaskResourceConfig(page, size, taskResourceConfigPageDto));
    }

	/**
	 * 更新任务资源配置数据
	 * @param taskResourceConfigEntity 待更新任务资源配置参数
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_CONFIG_UPDATE,msg="更新<${connectName}>任务资源配置数据")
	@CheckPermission("resource::taskResourceConfig::edit")
	@ApiOperation(value = "更新任务资源配置数据")
	@PutMapping()
	public ResultObj<Boolean> updateTaskResourceConfig(@RequestBody @Validated TaskResourceConfigEntity taskResourceConfigEntity) {
		return ResultObj.success(taskResourceConfigService.updateTaskResourceConfig(taskResourceConfigEntity));
	}

    /**
     * 测试数据库连接性
     */
    @CheckPermission("resource::taskResourceConfig::query")
    @ApiOperation(value = "测试数据库连接性")
    @PostMapping("/testConnection")
    public ResultObj<Boolean> testConnection(@RequestBody TaskResourceConfigEntity taskResourceConfigEntity) {
        ExceptionUtil.checkParam(StringUtils.hasLength(taskResourceConfigEntity.getConnectUrl())
            && StringUtils.hasLength(taskResourceConfigEntity.getConnectAccount())
            && StringUtils.hasLength(taskResourceConfigEntity.getConnectPassword()), "请确保数据库链接、账户名、密码完全输入！");
        ExceptionUtil.checkParam(
            taskResourceConfigEntity.getConnectUrl().startsWith(DataBaseTypeEnum.MYSQL.getUrlPrefix()),
            "请确保数据库链接地址输入正确！");
        return ResultObj.success(DynamicDataBase.testConnection(taskResourceConfigEntity.getConnectUrl(),
            taskResourceConfigEntity.getConnectAccount(), taskResourceConfigEntity.getConnectPassword()));
    }
}
