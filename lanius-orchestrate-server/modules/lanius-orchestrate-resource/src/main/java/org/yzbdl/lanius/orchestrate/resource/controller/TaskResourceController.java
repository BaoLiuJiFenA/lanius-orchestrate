/* Copyright (c) 2022 渝州大数据实验室
 *
 * Lanius is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.yzbdl.lanius.orchestrate.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.ExternalDirectoryTreeDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.ExternalFileDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourceDto;
import org.yzbdl.lanius.orchestrate.serv.dto.resource.TaskResourcePageDto;
import org.yzbdl.lanius.orchestrate.serv.entity.resource.TaskResourceEntity;
import org.yzbdl.lanius.orchestrate.serv.service.resource.TaskResourceService;
import org.yzbdl.lanius.orchestrate.common.annotation.auth.CheckPermission;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprId;
import org.yzbdl.lanius.orchestrate.common.annotation.log.OprLog;
import org.yzbdl.lanius.orchestrate.common.constant.UserOprEventConstant;
import org.yzbdl.lanius.orchestrate.common.result.ResultObj;

import java.util.List;

/**
 * 任务资源模块接口
 *
 * @author zhuhongji@yzbdl.ac.cn
 * @date 2022-04-08 16:49
 */
@Api(tags = "任务资源模块接口")
@RequestMapping("/resource/taskResource")
@RestController
@AllArgsConstructor
public class TaskResourceController {

	private final TaskResourceService taskResourceService;

	/**
	 * 添加任务资源数据
	 * @param taskResourceDto 待添加任务资源数据
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_ADD,msg="添加<${resourceName}>任务资源数据")
	@CheckPermission("resource::taskResource::edit")
	@ApiOperation(value = "添加任务资源")
	@PostMapping()
	public ResultObj<Boolean> add(@RequestBody @Validated TaskResourceDto taskResourceDto) {
		return ResultObj.success(taskResourceService.addTaskResource(taskResourceDto));
	}

	/**
	 * 逻辑删除任务资源数据
	 * @param id id
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_DELETE,msg="删除<${resourceName}>任务资源数据")
	@CheckPermission("resource::taskResource::edit")
	@ApiOperation(value = "逻辑删除任务资源")
	@DeleteMapping("/{id}")
	public ResultObj<Boolean> delete(@PathVariable @OprId(server = TaskResourceService.class) Long id) {
		return ResultObj.success(taskResourceService.deleteTaskResource(id));
	}

	/**
	 * 根据id获取任务资源数据
	 * @param id id
	 * @return 根据id获取任务资源数据
	 */
	@CheckPermission("resource::taskResource::query")
	@ApiOperation(value = "获取任务")
	@GetMapping()
	public ResultObj<TaskResourceEntity> getTaskResource(@RequestParam Long id) {
		return ResultObj.success(taskResourceService.getTaskResource(id));
	}

    /**
     * 分页查询任务资源数据
     * @param taskResourcePageDto taskResourcePageDto 分页参数
     * @return 分页数据
     */
    @CheckPermission("resource::taskResource::query")
    @ApiOperation(value = "分页查看任务资源")
    @PostMapping("query")
    public ResultObj<IPage<TaskResourceDto>> pageTaskResource(@RequestParam(defaultValue = "1") Integer page,
        @RequestParam(defaultValue = "20") Integer size,
        @RequestBody TaskResourcePageDto taskResourcePageDto) {
        return ResultObj.success(taskResourceService.pageTaskResource(page, size, taskResourcePageDto));
    }

	/**
	 * 更新任务资源数据
	 * @param taskResourceDto 待更新任务资源参数
	 * @return true|false
	 */
	@OprLog(value = UserOprEventConstant.TASK_RESOURCE_UPDATE,msg="更新<${resourceName}>任务资源数据")
	@CheckPermission("resource::taskResource::edit")
	@ApiOperation(value = "更新任务资源数据")
	@PutMapping()
	public ResultObj<Boolean> updateTaskResource(@RequestBody @Validated TaskResourceDto taskResourceDto) {
		return ResultObj.success(taskResourceService.updateTaskResource(taskResourceDto));
	}

	/**
	 * 获取外部资源树
	 * @param resourceConfigId	资源库id
	 * @return 外部资源树
	 */
	@CheckPermission("resource::taskResource::query")
	@ApiOperation(value = "任务资源树")
	@GetMapping(value = "getExternalResourceTree")
	public ResultObj<List<ExternalDirectoryTreeDto>> getExternalResourceTree(@RequestParam Long resourceConfigId) {
		return ResultObj.success(taskResourceService.buildExternalResourceTree(resourceConfigId));
	}

	/**
	 * 获取外部树目录下内容
	 * @param directoryId 目录id
	 * @param resourceConfigId 根目录为 0
	 * @return 外部树目录下内容
	 */
	@CheckPermission("resource::taskResource::query")
	@ApiOperation(value = "获取外部树目录下内容")
	@GetMapping(value = "getExternalResourceFile")
	public ResultObj<ExternalFileDto> getExternalResourceFile(@RequestParam(required = false, defaultValue = "0") Long directoryId,
	                                                          @RequestParam Long resourceConfigId,
	                                                          @RequestParam(required = false) Integer resourceType) {
		return ResultObj.success(taskResourceService.getExternalResourceFile(directoryId, resourceConfigId, resourceType));
	}

}
