<div align="center" style="margin-top: 10px">
   <img alt="SmartSQL" src="docs/img/logo.png" style="width: 75px">
	<h2 align="center">Lanius Orchestrate</h2>
</div>

<p align="center">
<img alt="Version" src="https://img.shields.io/badge/Version-1.0-red.svg">
<img alt="Java" src="https://img.shields.io/badge/JDK-11-yellow.svg">
<img alt="Spring Boot" src="https://img.shields.io/badge/Spring%20Boot-2.6.3-green.svg">
<img alt="Maven" src="https://img.shields.io/badge/Maven-3.6-brightgreen.svg">
<img alt="License" src="https://img.shields.io/badge/License-MulanPSL%202.0-blue.svg">
</p>


 ## 一、研发背景

数据继土地、劳动力、资本之后成为新兴的生产要素，各国政府相继制定战略规划，加大对数据汇聚、分析、安全等方面的投入，我国各级政府也不断出台政策，在大数据治理方面给予支持。

渝州大数据实验室是重庆市九龙坡区人民政府与北京大数据先进技术研究院共同建设的新型科研创新实体。实验室由我国知名院士领衔，面向国家大数据建设的基础通用需求、前沿关键技术和典型应用场景，推进大数据技术研发应用、成果转化与生态构建，为国家大数据建设提供支撑。

## 二、产品简介

“Lanius大数据治理平台”是一款由渝州大数据实验室研发推出的数据ETL平台，拥有生产级、易使用、智能化的特点。以推动国内数据治理发展为目标，向个人、政企单位免费开源。
产品主要由设计器（Lanius Desgin）、执行器（Lanius Executor）和调度器（Lanius Orchestrate）三部分构成，其中：

- Lanius Desgin用于开发人员设计数据ETL任务，Lanius Executor用于执行已设计好的ETL任务，用户加入QQ群155800768后，都可以通过群文件免费下载和使用。
- Orchestrate用于运维人员调度和监控任务，目前已经通过Gitee平台完全开源。

<img alt="orchestrate" src="docs/img/orchestrate.jpg" width=500 />

## 三、产品特色

### 1、完全开源

Orchestrate代码完全开源，遵循木兰2.0开源协议，可放心用于商业项目中。

### 2、调度灵活

实现调度任务的单个或批次的新增、删除、修改、查询、启动、停止等操作，任务和转换/作业相关联，实现转换/作业的自动化调度，支持对任务执行结果进行自动分析，支持根据任务结果进一步触发其他任务事件。

### 3、运维方便

提供详细的指标监视页面，以便掌控全局；提供邮件、短信等多种主动通知方式，无需专人值守；汇总各个节点任务执行日志，提供统一的渠道排查相关问题；让运维工作不再困难。

### 4、高效稳定

在某些大型项目中，需要部署多套程序来应对高频、量大的数据处理需求。我们的Orchestrate支持同时管理多台服务器、多个ETL任务节点，提高数据处理的效率和稳定性，更贴近生产需求。

## 四、主要功能

### 1、调度总览

帮助用户对系统资源的健康情况和任务执行情况做到全局掌控，提供服务器、执行器、资源库健康统计能力，提供资源类型统计能力，提供任务状态统计能力。支持用户通过统计卡片进行快捷跳转，帮助用户快速定位系统资源和任务。

<img alt="orchestrate" src="docs/img/dashboard.png" width=500 />


### 2、资源管理

为集群的各个资源的提供管理能力，包括服务器、调度器、执行器、资源库、任务资源。提供定时检测资源健康的能力，加强任务调度运行环境的把控力。

<img alt="orchestrate" src="docs/img/resource_list.png" width=500 />

<img alt="orchestrate" src="docs/img/resource_edit.png" width=300 />

### 3、任务编排

为满足任务自动调度的需求，系统提供任务编排管理模块。可以将需要自动调度的任务提前在此进行编排，将任务的调度周期、ETL 资源、日志等级、名称、参数等相关信息录入到系统，系统将根据设置的调度周期重复执行任务。支持按任务的业务管理需求将任务调度计划进行层级化的分组管理，并提供添加、修改、查询、移动、删除等界面管理功能。

对于已录入到系统中的任务，系统提供启用/禁用、修改、手动启用、删除等能力。

<img alt="orchestrate" src="docs/img/task_list.png" width=500 />

### 4、任务实例

每次执行任务系统都会对应生成一条实例数据，任务调度系统具备实时监控任务实例整个执行过程的能力。数据工程师可以在这里查看每条任务实例调度的实际执行的 ETL 资源、执行器、消耗时间等信息。为了帮助数据工程师快速查找历史记录，系统支持按照任务名称、资源名称、执行器名称、执行状态、时间范围过滤任务实例。除此之外，数据工程师还可以通过详情，查看实例的工作流程图、执行过程中记录的日志以及步骤度量等详细信息，有助于帮助数据工程师分析此次任务的执行情况，以及查找任务工作流程中需要优化的节点。

任务执行如果出现执行失败或者监控异常等情况，数据工程师可以返回任务编排管理模块，将任务进行调整后，通过手动执行的方式，再次尝试执行任务。

<img alt="orchestrate" src="docs/img/task_instance_list.png" width=500 />

### 6、日志管理

系统提供自动记录用户操作事件的能力，让用户的每一步操作都有迹可循，有效确保操作环境的安全。

<img alt="orchestrate" src="docs/img/user_log.png" width=500 />

## 五、本地编译文档

<a target='_blank' href ='docs/Build.md'>点击查看本地编译文档</a>

## 六、开发文档

<a target='_blank' href ='docs/Develop.md'>点击查看开发文档</a>

## 七、部署文档

<a target='_blank' href ='docs/Deploy.md'>点击查看部署文档</a>    
快速体验，请参考《<a target='_blank' href ='docs/autorun/AUTORUN.md'>自动部署手册</a>》

## 八、更新日志

<a target='_blank' href ='docs/ReleaseNote.md'>点击查看更新日志</a>

## 九、历史下载

## 十、FAQ

<a target='_blank' href ='docs/FAQ.md'>点击查看FAQ</a>

## 十一、联系我们

电话：023-68516919

QQ群：<a href="https://qm.qq.com/cgi-bin/qm/qr?k=2JFFIbTFGQwyFFmfiwS-Tqn0OysAHPIg&authKey=QGrhB288t7TDopZbz5x1z3y8UZhR1%2FQ7U1H94H5Q62F58htTXsEtP0bCqUstqOch&noverify=0&group_code=155800768">155800768</a> 

QQ群二维码：<img alt="orchestrate" src="docs/img/LaniusQQqun.jpg" width="100"> &nbsp;&nbsp;&nbsp;&nbsp;公众号二维码：<img alt="orchestrate" src="docs/img/gongzhonghao.png" width="100">




 